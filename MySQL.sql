## MAJ 05/10/2018

CREATE TABLE `fam` (
  `fam` varchar(20) NOT NULL UNIQUE,
  `libelle` tinytext NOT NULL,
  `color` varchar(9) NOT NULL DEFAULT '#FFFFFF'
);

ALTER TABLE `fourniture`
ADD `fam` varchar(20) NULL,
ADD FOREIGN KEY (`fam`) REFERENCES `fam` (`fam`);

ALTER TABLE `tache`
ADD `compo` text NULL COMMENT 'tache composi' AFTER `uploadFile`;

ALTER TABLE `msg_topics`
ADD `topic_see` text NULL AFTER `topic_to`;

UPDATE `msg_topics` SET `topic_see` = '[]';
<?php
//print_r($_SESSION['user']);
//exit;
$user_id = $_SESSION["user"]->id_user;
$draft   = strtoupper(sql_inj($_GET['draft'],""));
$r   = new \stdClass;
$frm = $fw->get_json(true);

if (isset($_GET['save']) && $draft != "")
{
  file_put_contents("../uploads/draft/$draft.json", json_encode($frm));
}

if (isset($_GET['load']))
{
  if (file_exists("../uploads/draft/$draft.json"))
    $r = json_decode( file_get_contents("../uploads/draft/$draft.json"));
}

echo json_encode($r, JSON_PRETTY_PRINT);
<?php

$r              = new \stdClass;
$post           = $fw->get_json(true);

$user_id        = $_SESSION["user"]->id_user;
$topic_id       = sql_inj($post->topic_id);
$topic_subject  = sql_inj($post->subject);
$topic_to       = sql_inj($post->to);
$topic_by_close = sql_inj($post->topic_by_close);
$topic_to_close = sql_inj($post->topic_to_close);
$topic_by       = $user_id;
$post_content   = sql_inj($post->content);

// insert new topic
if( !$topic_id && $topic_to!=null && $topic_subject && $post_content){

  if ( intval($topic_to)==0 )
  {
    $topic_to = $fw->getUsersOf($topic_to,false);
  }
  else
  {
   $topic_to = '["'.$topic_to.'"]'; 
  }

  $topics = $fw->fetchAll("INSERT INTO msg_topics (topic_subject, topic_by, topic_to, topic_by_close, topic_to_close) VALUES ('$topic_subject', '$topic_by', '$topic_to', 'n', 'n')",true);

  $topic_id = $topics['id'];
  $post = $fw->fetchAll("INSERT INTO msg_posts (post_content, post_topic, post_by) VALUES ('$post_content', '$topic_id', '$topic_by')",true);

  $r = $topics;
  

// replay to topic N
}else if( $topic_id && $post_content){

  $fw->fetchAll("UPDATE msg_topics SET `topic_by_close`='n', `topic_to_close`='n', `topic_see`='[]', `topic_date`=NOW()  WHERE `topic_id`='$topic_id'",true);
  $r = $fw->fetchAll("INSERT INTO msg_posts (post_content, post_topic, post_by) VALUES ('$post_content', '$topic_id', '$topic_by')", true);

// change status of topic N
}else if( $topic_id && $topic_by && $topic_by_close && $topic_to_close && isset($_GET['close']) ){

  $topic_by = sql_inj( $post->topic_by );

  if (strlen($post->post_content)>0){
    $post_content = sql_inj($post->post_content);
    $fw->fetchAll("INSERT INTO msg_posts (post_content, post_topic, post_by) VALUES ('$post_content', '$topic_id', '$user_id')", true);
  }

  $r = "id ".$user_id." ";
  $r .= "tpid ".$topic_id." ";

  $topic_to = json_decode( str_replace( '\"', '"', sql_inj($post->topic_to) ) );

  if ( $topic_by == $user_id && in_array($user_id, $topic_to, true) ){
    $fw->fetchAll("UPDATE msg_topics SET `topic_by_close`='d',`topic_to_close`='$user_id' WHERE `topic_id`='$topic_id'", true);
    $r .= "topic_by_close";

  }else if ( $topic_by == $user_id ){
    $fw->fetchAll("UPDATE msg_topics SET `topic_by_close`='d' WHERE `topic_id`='$topic_id'", true);
    $r .= "topic_by_close";

  }else{
    $fw->fetchAll("UPDATE msg_topics SET `topic_to_close`='$user_id' WHERE `topic_id`='$topic_id'", true);
    $r .= "topic_to_close";

  }  

// change status of topic N
}else if( $topic_id && isset($_GET['see']) ){
  $tpk_see_list = $fw->fetchAll("SELECT `topic_see` FROM msg_topics WHERE `topic_id`='$topic_id'")[0];

  
  if (!$tpk_see_list->topic_see || $tpk_see_list->topic_see==null || $tpk_see_list->topic_see=="")
    $tpk_see_list->topic_see = "[]";
  
  $r = json_decode( $tpk_see_list->topic_see );

  $found = false;
  foreach ($r as $el) {
    if ($el->u == $user_id){
      $el->d = date('Y/m/d h:i:s');
      $found = true;
      break;
    }
  }
      
  if (!$found){
    $see = new \stdClass;
    $see->u = $user_id;
    $see->d = date('Y/m/d h:i:s');
    array_push($r, $see);
  }

  $fw->fetchAll("UPDATE msg_topics SET `topic_see`='".json_encode($r)."' WHERE `topic_id`='$topic_id'",true);

// list topic
}else if( !$topic_id ){

  if (isset($_GET['news'])){

    $r = $fw->fetchAll("
      SELECT
        topic_id, topic_subject, post_date, CONCAT(utilisateur.nom,' ',utilisateur.pnom) as fullname 
      FROM
        msg_topics, msg_posts, utilisateur 
      WHERE
        msg_posts.post_topic = msg_topics.topic_id
      AND
        msg_posts.post_by = utilisateur.id_user
      AND
        msg_topics.topic_by_close != 'd'
      AND
        msg_topics.topic_to_close != 'd'
      AND
      (
        `topic_by` = $user_id
      OR
        `topic_to` LIKE '%\"$user_id\"%'
      )
      ORDER BY topic_date DESC
      LIMIT 99;
    ");

  }else{

    $r = $fw->fetchAll("
      SELECT
        msg_topics.*, CONCAT(utilisateur.nom,' ',utilisateur.pnom) as fullname, utilisateur.avatar, 
        IF(`topic_by`=$user_id,true,false) as iam_sender,
        IF(`topic_to_close`='n',null, (SELECT CONCAT(utilisateur.nom,' ',utilisateur.pnom) as fullname FROM utilisateur WHERE utilisateur.id_user=msg_topics.topic_to_close) )  as topic_to_close_name

      FROM
        msg_topics, utilisateur
      WHERE 
        utilisateur.id_user=msg_topics.topic_by
      AND
        (
          `topic_by` = $user_id
        OR
          `topic_to` LIKE '%\"$user_id\"%'
        )
      ORDER BY topic_date DESC
      LIMIT 99;
    ");

    foreach ($r as $key => $value) {
      $p = $fw->fetchAll("
        SELECT
          msg_posts.*, CONCAT(utilisateur.nom,' ',utilisateur.pnom) as fullname, utilisateur.avatar
        FROM
          msg_posts, utilisateur
        WHERE 
          utilisateur.id_user=msg_posts.post_by
        AND
          `post_topic` = $value->topic_id
      ");
      $r[$key]->posts = $p;

      $r[$key]->topic_to_user = [];
      $topic_to = json_decode($value->topic_to);

      if (!in_array($r[$key]->topic_by,$topic_to))
        array_push($topic_to, $r[$key]->topic_by);

      foreach ($topic_to as $ttkey => $ttvalue) {
        $usr = $fw->fetchAll("SELECT nom,pnom,id_user,avatar,ch as depart, IF(utilisateur.date_act < NOW() - INTERVAL 30 SECOND, false, true) AS `on` from utilisateur where id_user=$ttvalue")[0];

        $usr->see = false;
        //var_dump();
        if ($r[$key]->topic_see){
          foreach (json_decode($r[$key]->topic_see) as $el) {
            if ( isset($el->u) && isset($usr->id_user) && $el->u == $usr->id_user){
              $usr->see = $el->d;
              //print_r($usr);
              break;
            }
          }          
        }
        array_push($r[$key]->topic_to_user, $usr);
      }

    }
  }

}else{
  echo "error";
}



echo json_encode($r, JSON_PRETTY_PRINT);
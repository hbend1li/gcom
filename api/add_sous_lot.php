<?php
session_start();

$libelle_sous_lot = htmlspecialchars($_POST['libelle_sous_lot']);
$Num_lot = intval($_POST['Num_lot']);

$msg=null;
 
if(isset($libelle_sous_lot) AND isset($Num_lot)){
  if($libelle_sous_lot != '' AND $Num_lot > 0)
  {
    include '../modele/conne.php';
  
    $req = "SELECT max(Num_sous_lot) FROM sous_lot";
    $reponse = $bdd->prepare($req);
    $reponse->execute();
    $donnee = $reponse->fetchAll(PDO::FETCH_ASSOC);
    $num_sous_lot_max = $donnee[0]['max(Num_sous_lot)'] + 1;

    $id_user = $_SESSION["user"]["id_user"];

    $req = "INSERT INTO sous_lot ( Num_sous_lot, libelle_sous_lot, Num_lot, user_add_sous_lot) 
            VALUES ( $num_sous_lot_max, \"$libelle_sous_lot\", $Num_lot, $id_user )";
    
    $reponse = $bdd->prepare($req);
    $donnee=$reponse->execute();
    if($donnee)
    {
      $msg['msg']='ok';
    }
    else
    {
      $msg['msg']='Erreur non definie';
      $msg['sql']=$req;
      $msg['debug']=$reponse->errorInfo();
    }
  }
  else
  {
    $msg['msg']='Erreur non definie';
    $msg['debug']=$_POST;
  }
}
else
{
  $msg['msg']='Nom ou Groupe non definie';
  $msg['debug']=$_POST;
}

echo json_encode($msg);
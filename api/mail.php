<?php

$r            = new \stdClass;
$user_id      = $_SESSION["user"]->id_user;
$mail         = $fw->get_json(true);

$id_mail      = sql_inj($_GET['mail'],0);
$destination  = sql_inj($mail->to);
$obj          = sql_inj($mail->obj);
$core         = sql_inj($mail->core);
//$sts          = sql_inj($mail->sts,null);
$replayTo     = sql_inj($mail->replayTo,null);


if (isset($_GET['list'])){
  $r = $fw->fetchAll("
    SELECT
      mail.*, CONCAT(utilisateur.nom,' ',utilisateur.pnom) as fullname
    FROM
      mail, utilisateur
    WHERE
      (
        (
          SELECT
            acl
          FROM
            utilisateur
          WHERE
            `id_user`=$user_id
        )
        LIKE
          CONCAT( '%\"',`receiver`,'\":true%' )
        OR
          `receiver`=$user_id
      )
    AND
      ( `sr` IS NULL OR `sr`!='d' )
    AND
      utilisateur.id_user=mail.sender


  ");

}

// Change Status MAIL
else if( $id_mail!=0 && (isset($_GET['ss']) || isset($_GET['sr'])) ){
  $stsChange = "";
  if ( isset($_GET['ss']) && ( $_GET['ss']== '' || $_GET['ss']== 'r' || $_GET['ss']== 'c' || $_GET['ss']== 'd' ))
    $stsChange = " `ss`='$_GET[ss]' ";
  if ( isset($_GET['sr']) && ( $_GET['sr']== '' || $_GET['sr']== 'r' || $_GET['sr']== 'c' || $_GET['sr']== 'd' ))
    $stsChange = " `sr`='$_GET[sr]' ";

  $r->pdo = $fw->fetchAll("UPDATE mail SET $stsChange WHERE id_mail = $id_mail;",true,true);
}

// Got MAIL
else if ($id_mail!=0)
{
  $r->pdo = $fw->fetchAll("
    SELECT
      mail.*, CONCAT(utilisateur.nom,' ',utilisateur.pnom) as fullname, utilisateur.avatar
    FROM
      mail, utilisateur
    WHERE
      id_mail=$id_mail
    AND
      utilisateur.id_user = mail.sender
  ",true,true);
}

// Send MAIL
else if ($destination && $obj && $core)
{
  $r->pdo = $fw->fetchAll("INSERT INTO mail (sender, receiver, obj, core, ss) VALUES ($user_id, '$destination', '$obj', '$core', '')",true,true);
}

// Replay To MAIL
else if ($replayTo && $core)
{
  $sndMail = $fw->fetchAll("SELECT * FROM mail WHERE id_mail = $replayTo;")[0];
            // Close this Mail and Create new
            $fw->fetchAll("UPDATE mail SET sr = 'c' WHERE id_mail = $replayTo;");

  $r->pdo = $fw->fetchAll("INSERT INTO mail (sender, receiver, obj, core, ss) VALUES ($user_id, '$sndMail->id_user', '$sndMail->obj', '$core', '')",true,true);
  
}

// Else What's NEW
else {
  $r = $fw->fetchAll("
    SELECT
      mail.*, CONCAT(utilisateur.nom,' ',utilisateur.pnom) as fullname
    FROM
      mail, utilisateur
    WHERE
      (
        (
          SELECT
            acl
          FROM
            utilisateur
          WHERE
            `id_user`=$user_id
        )
        LIKE
          CONCAT( '%\"',`receiver`,'\":true%' )
        OR
          `receiver`=$user_id
      )
    AND
      ( `sr` IS NULL OR `sr`!='d' )
      
    AND
      utilisateur.id_user=mail.sender
  ");

  $fw->fetchAll("
    UPDATE mail SET sts='read' WHERE
    (
      ( SELECT acl FROM utilisateur WHERE id_user=$user_id ) LIKE CONCAT( '%\"',`receiver`,'\":true%' )
      OR `receiver`=$user_id
    )
    user_id    AND sts is NULL;");
}  

echo json_encode($r, JSON_PRETTY_PRINT);
<?php
$user_id = $_SESSION["user"]->id_user;
$r   = new \stdClass;
$frm = $fw->get_json(true);

$id_      = sql_inj($frm->id_user,null);
$username = sql_inj($frm->username,null);
$password = sql_inj($frm->password,null);
$nom      = sql_inj($frm->nom,null);
$pnom     = sql_inj($frm->pnom,'');
$email    = sql_inj($frm->email,'');
$ch       = sql_inj($frm->ch,'');
$dir      = sql_inj($frm->dir,'');
$mob      = sql_inj($frm->mob,'');
$tel      = sql_inj($frm->tel,'');
$fax      = sql_inj($frm->fax,'');
$adr      = sql_inj($frm->adr,'');
$sig      = sql_inj($frm->sig,'');
$avatar   = sql_inj($frm->avatar,'./img/avatar.png');
$acl	  = isset($frm->acl)?json_encode($frm->acl):'{}';


// New USER ////////////////////////////////////////////////////////////////////////////
if ( $id_ == 'new' && $username && $password && $nom && strlen($password)>1){
  $password = base64_encode($password);
  $r = $fw->fetchAll("INSERT INTO utilisateur (username, password, nom, pnom, email, ch, dir, mob, tel, fax, adr, sig, avatar, acl) VALUES ('$username', '$password', '$nom', '$pnom', '$email', '$ch', '$dir', '$mob', '$tel', '$fax', '$adr', '$sig', '$avatar', '$acl');",true,true);

// Update USER ////////////////////////////////////////////////////////////////////////////
}else if ( intval($id_) != 0 && $username && $nom && (($password && strlen($password)>1) || !$password)){
  if ($password)
    $password = "password = '".base64_encode($password)."',";
  
  $r = $fw->fetchAll("UPDATE utilisateur SET username = '$username', $password nom = '$nom', pnom = '$pnom', email = '$email', ch = '$ch', dir = '$dir', mob = '$mob', tel = '$tel', fax = '$fax', adr = '$adr', sig = '$sig', avatar = '$avatar', acl='$acl' WHERE id_user = $id_;",true,true);

// Delete USER ////////////////////////////////////////////////////////////////////////////
}else if(intval($_GET['user']) != 0 && isset($_GET['del'])){
  $id_ = intval($_GET['user']);
  $r = $fw->fetchAll("DELETE FROM utilisateur WHERE id_user = $id_;");

// View USER ////////////////////////////////////////////////////////////////////////////
}else if(intval($_GET['user']) != 0){
  $id_ = intval($_GET['user']);
  $r = $fw->fetchAll("SELECT id_user, username, nom, pnom, email, ch, dir, mob, tel, fax, adr, sig, avatar, acl, date_act FROM utilisateur WHERE id_user = $id_;");
  foreach ($r as $ele)
  	$ele->acl = json_decode($ele->acl);


}else{

}

echo json_encode($r, JSON_PRETTY_PRINT);
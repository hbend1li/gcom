<?php 

// #Define
define("Allow","1");
define("Denied","0");

function isallow($parram)
{
    //if (isset($_SESSION["user"]->acl) AND in_array($parram,$_SESSION["user"]->acl) )
	if (isset($_SESSION["user"]->acl->$parram) AND $_SESSION["user"]->acl->$parram == true )
		return true;
	else
		return false;
}

function curl_get_contents($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

function quotes(&$val, $quotes="'"){
    return isset($val) ? $quotes.sql_inj($val,'').$quotes : 'null';
}

function sql_inj(&$val, $default = null){
    $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
    $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
    return isset($val) ? str_replace($search, $replace, $val) : $default;
}

function issetor(&$var, $default = false) {
    return isset($var) ? $var : $default;
}


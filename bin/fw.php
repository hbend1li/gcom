<?php
//date_default_timezone_set('UTC');
date_default_timezone_set('Africa/Algiers');
setlocale(LC_MONETARY, 'dz_DZA');

//error_reporting(E_ALL);
//ini_set("display_errors", 1);
//ini_set('session.cookie_domain', '.ogcom.com' );

//session_cache_limiter('nocache');
//session_cache_expire(10);
session_start();

require_once('define.php');
require_once('include.php');
require_once('db-connect.php');

class FireWorks{

    private static $databases;
    private $connection;

    public function __construct($connDetails){
        if(!is_object(self::$databases[$connDetails])){
            list($host, $port, $user, $pass, $dbname) = explode('|', $connDetails);
            $dsn = "mysql:host=$host;port=$port;dbname=$dbname";
            self::$databases[$connDetails] = new PDO($dsn, $user, $pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        }
        $this->connection = self::$databases[$connDetails];
       
        //$this->signupdate();
    }

    // RUN SQL =========================================================
    public function fetch($sql){
        $args = func_get_args();
        array_shift($args);
        $statement = $this->connection->prepare($sql);
        $statement->execute($args);
        return $statement->fetch(PDO::FETCH_OBJ);
    }

    public function fetchAll($sql,$result = null,$debug = null){
        $args = func_get_args();
        array_shift($args);
        $statement = $this->connection->prepare($sql);
        $statement->execute($args);

        if ($result){
            $result = array();
            $result['sqlState']  = $statement->errorInfo()[0];
            $result['errorCode'] = $statement->errorInfo()[1];
            $result['message']   = $statement->errorInfo()[2];
            $result['id']        = $this->connection->lastInsertId();
            $result['result']    = $statement->fetchAll(PDO::FETCH_OBJ);

            if ($this->policy('debug') || $debug === true)
             $result['sqlquery'] = $sql;

            return $result;
        }else
            return $statement->fetchAll(PDO::FETCH_OBJ);
    }



    // SQL Generate INSERT or UPDATE ===================================
    public function sql_gen($table, $fields = array(), $id="")
    {
        if ( strtoupper($id) =="SELECT"){ // SELECT x,x,x,x,
            $keys = "";
            foreach ( $fields as $key )
            {
                $keys .= "`$key`,";
            }
            $keys = substr($keys,0,-1);
            return "SELECT $keys FROM $table WHERE `id`='$id'";

        }else if (intval($id)==0){ // INSERT
            $keys = "";
            $vals = "";
            foreach ( $fields as $key => $val )
            {
                $keys .= "`". $this->sql_inj($key) ."`,";
                $vals .= $this->reformat_date($key,$val).",";
            }
            $keys = substr($keys,0,-1);
            $vals = substr($vals,0,-1);
            return "INSERT INTO $table ($keys) VALUES ($vals)";

        }else{ // UPDATE
            $element = "";
            foreach ( $fields as $key => $val )
            {
                $element .= "`". $this->sql_inj($key) ."`=". $this->reformat_date($key,$val) .",";
            }
            $element = substr($element,0,-1);
            return "UPDATE $table SET $element WHERE `id`='$id'";
        }
    }

    // SQL Injection ===================================================
    function sql_inj($value)
    {
        $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
        $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
        return str_replace($search, $replace, $value);
    }

    // Reformat value if field is date =================================
    public function reformat_date($key, $val)
    {
        if (substr($key,0,5) =="date_")
        {
            $val = "'".date("Y-m-d", strtotime( substr($val, 0, strpos($val, '('))))."'";
            if ($val=="'1970-01-01'") $val="null";
        }else{
            $val = "'".$this->sql_inj($val)."'";
        }
        return $val;
    }

    // AVATAR ==========================================================
    public function gravatar( $email, $img = false, $s = 80, $d = 'mm', $r = 'g', $atts = array() ) {
        //

        $url = 'https://www.gravatar.com/avatar/' . md5( strtolower( trim( $email ) ) );
        //$url = './img/mm.png';

        if ( $img ) {
            $url .= "?s=$s&d=$d&r=$r";

            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }


        return $url;
    }

    // SIGNIN ==========================================================
    public function signin( $username=null , $password=null ) {
        global $_SESSION;
        $ret = false;
        if ( $username==null && $password==null )
        {
            if ( isset($_SESSION['user']) )
                $ret = true;
        }else{
            $username = $this->sql_inj($username);
            $password = $this->sql_inj($password);
            $pwd      = base64_encode($password); //sha1($password);
            $result =  $this->fetchAll("SELECT * FROM $this->tb_user WHERE ( username='$username' OR email='$username' ) AND password='$pwd'");
            if (count($result)>0){
                $_SESSION['user'] = $result[0];
                $this->signupdate();

                $this->log( "SIGNIN ".$username ) ;
                $ret = true;
            }else{
                $this->signout();
                $this->log( "ACCESS DENIED / $username / $password / " ) ;
            }
        }

        return $ret;
    }

    // SIGNOUT =========================================================
    public function signout() {
        global $_SESSION;
        if (isset($_SESSION['user']))
        {
            $this->log( "SIGNOUT / ".$_SESSION['user']->username." / " ) ;
            $_SESSION['user'] = null;
            unset($_SESSION);
        }
        session_destroy();
    }

    // SIGNUPDATE =========================================================
    public function signupdate() {
        global $_SESSION;
        if ( isset($_SESSION['user']) && $_SESSION['user']->id_user){
            $id_user = $_SESSION['user']->id_user;
            $this->fetch("UPDATE `utilisateur` SET `date_act` = now() WHERE `id_user` = '$id_user'");
            return true;
        }else{
            return false;
        }
    }

    // ACCESS =========================================================
    public function policy($acl) {
        global $_SESSION;
        return (isset($_SESSION['user']) && isset($_SESSION['user']->policy->$acl) )?$_SESSION['user']->policy->$acl:false;
    }

    public function get_json($decode = false)
    {
        $putjson = fopen("php://input", "r");
        $json = "";
        while (!feof($putjson))
            $json .=fgets($putjson);
        fclose($putjson);
        if (!$decode)
            return $json;
        else
            return json_decode($json);
    }

    public function to_html($txt)
    {
        $txt = str_replace("'" ,"&#8217;" ,$txt);
        $txt = str_replace(" " ,"&nbsp;"  ,$txt);
        $txt = str_replace("\r\n" ,"<br>"  ,$txt);
        return($txt);
    }

    public function next_id($table)
    {
        return $this->fetchAll("
            SELECT AUTO_INCREMENT
            FROM information_schema.tables
            WHERE table_name = '$table'
            AND table_schema = DATABASE( )
        ")[0]->AUTO_INCREMENT;
    }

    public function getUser($id_user)
    {
        return $this->fetchAll("SELECT id_user, username, nom, pnom, email, ch, dir, mob, tel, fax, adr, sig, avatar FROM utilisateur WHERE `id_user`=$id_user")[0];
    }

    public function getUsersOf($group,$array=false)
    {
        $t = $this->fetchAll("SELECT id_user FROM utilisateur WHERE `acl` LIKE '%\"$group\":true%'");
        $result = [];
        foreach ($t as $ele=>$val) {
            if ($val) array_push($result, $val->id_user);
        }
        return $array?$result:json_encode($result);
    }

    public function getGroupsOf($id_user,$array=false)
    {
        //$t = ;
        $result = [];
        foreach (json_decode($this->fetch("SELECT acl FROM utilisateur WHERE `id_user`=$id_user")->acl) as $ele=>$val) {
            if ($val) array_push($result, $ele);
        }
        return $array?$result:json_encode($result);
    }

    public function getGroup($id_user)
    {
        $gr = json_decode( $this->fetch("SELECT acl FROM utilisateur WHERE `id_user`=$id_user")->acl );
        if (isset ($gr->bat)        && $gr->bat)        return 'bat';
        if (isset ($gr->gc)         && $gr->gc)         return 'gc';
        if (isset ($gr->cet)        && $gr->cet)        return 'cet';
        if (isset ($gr->tc)         && $gr->tc)         return 'tc';
        if (isset ($gr->dpi)        && $gr->dpi)        return 'dpi';
        if (isset ($gr->admin)      && $gr->admin)      return 'admin';
        if (isset ($gr->programmer) && $gr->programmer) return 'programmer';
        return null;
    }


}
<div ng-controller="TodoCtrl" >

  <div class="ui clearing yellow inverted segment print_ignore">

    <h3>LISTE DES UTILISATEURS</h3>

  </div>


  <table class="ui compact selectable red table">
    <thead>
      <tr>
        <th>Utilisateur</th>
        <th>Droit d'Access</th>
        <th class="right aligned">
          <a class='ui mini red icon button'
            data-tooltip="Ajouter un Utilisateur"
            data-inverted=""
            data-position="left center"
            href="?p=utilisateur/profile" 
            >
            <i class='add icon'></i>
          </a>          
        </th>
      </tr>
    </thead>
    <tbody>
      <tr
        ng-repeat="ele in utilisateur | filter:srh_"
        ng-cloak
        >
        <td>


<div class="ui feed">
  <div class="event">
    <div class="label">
      <div class="keepalive" ng-class="{on: ele.on == '1'}"></div>
      <img src="{{ele.avatar}}">
    </div>
    <div class="content">
      <div class="summary">
        <a class="user" href="?p=utilisateur/profile&id={{ele.id_user}}">{{ele.nom}} {{ele.pnom}}</a>
      </div>
      <div class="meta">
        <a class="like">
          <i class="fa fa-at"></i> {{ele.username}} - {{ele.email}}
        </a>
      </div>
    </div>
  </div>

</div>



        </td>
        <td>
          <i  class="fa fa-2x"
              ng-class="
                ele.acl.enable ? (
                  !ele.acl.programmer ? (
                    !ele.acl.admin ? (
                      !ele.acl.user_plus ? (
                        !ele.acl.user ? (
                          'fa-unlock red'
                        ) : 'fa-group yellowgreen'
                      ) : 'fa-user-plus orange'
                    ) : 'fa-shield red'
                  ) : 'fa-code-fork red'
                ) : 'fa-unlock lightgrey'
              "
              title="{{
                ele.acl.enable ? (
                  !ele.acl.programmer ? (
                    !ele.acl.admin ? (
                      !ele.acl.user_plus ? (
                        !ele.acl.user ? (
                          'COMPTE ACTIVER ET NON ASSIGNER A UN GROUPE'
                        ) : 'UTILISATEUR SIMPLE'
                      ) : 'UTILISATEUR AVANCER'
                    ) : 'ADMINISTRATEUR'
                  ) : 'DEVELOPER, DEBUGER'
                ) : 'COMPTE DESACTIVER'
              }}"
          ></i>
          <i class="fa fa-2x fa-thumb-tack"  ng-class="ele.acl.method?'':'disable'"      title="METHODISTE"></i>
          <i class="fa fa-2x fa-building"    ng-class="ele.acl.bat?'':'disable'"         title="BATIMENT BAT"></i>
          <i class="fa fa-2x fa-street-view" ng-class="ele.acl.gc?'':'disable'"          title="GENIE CEVILE G.C"></i>
          <i class="fa fa-2x fa-cubes"       ng-class="ele.acl.dpi?'':'disable'"         title="DÉPARTEMENT PRODUCTION ET INFRASTRUCTURE D.P.I"></i>
          <i class="fa fa-2x fa-sitemap"     ng-class="ele.acl.cet?'':'disable'"         title="CORPS D'ETAT TECHNIQUES C.E.T"></i>
          <i class="fa fa-2x fa-at"          ng-class="ele.acl.tc?'':'disable'"          title="TECHNIQUO COMMERCIALE T.C"></i>

        </td>
        <td class="right aligned">{{ele.date_act | date}}</td>
      </tr>
    </tbody>
  </table>
</div>


<script language="javascript"> app.controller('TodoCtrl', function($scope, $http) {

  $http.get('api/?list=utilisateur')
    .then(function(res){
      res.data.forEach(function(element,index){
          element.acl = JSON.parse(element.acl);
          element.date_act = new Date(element.date_act);
      });
      $scope.utilisateur = res.data;
      console.log($scope.utilisateur);
    });

}); </script>
<div ng-controller="TodoCtrl" class="ui text container">

  <div class="ui segment">
    <div class="ui left dividing rail">
      
      <div class="ui card">
        <div class="image" href="#">
          <img src="img/mail.jpg">
        </div>
      </div>
      
      <div class="ui fluid facebook button" ng-click="newPost(true)">Nouvelle notification</div>

      <div class="ui blue card">
        <div class="content">
          <div class="header" href="#">NOTIFICATION</div>
          <div class="meta">
            <p></p>
            <div class="ui relaxed divided list">
                <div class='item' ng-repeat="(key, value) in topics" ng-show="value.topic_by_close=='n' || value.topic_to_close=='n'" ng-cloak>
                  <i  class='large envelope outline red middle aligned icon'
                      ng-class="{disabled : value.topic_by_close!='n' || value.topic_to_close!='n' }">
                  </i>
                  <div class='content'>
                    <a class='header' ng-click="viewPost(value.topic_id,true)">
                      <div ng-class="{del : value.topic_by_close!='n' || value.topic_to_close!='n' }">{{value.topic_subject}}</div>
                    </a>
                    <div class='description'>{{value.fullname}}</div>
                  </div>
                </div>
            </div>
          </div>
        </div>

      </div>

      <div class="ui fluid basic button" ng-click="archive=!archive">Notification Archives</div>

      <div class="ui card" ng-show="archive" ng-cloak>
        <div class="content">
          <div class="header" href="#">NOTIFICATION ARCHIVES</div>
          <div class="meta">
            <p></p>
            <div class="ui relaxed divided list">
                <div class='item' ng-repeat="(key, value) in topics | filter:{topic_by_close:'!n', topic_to_close:'!n'}" ng-cloak>
                  <i class='large envelope outline middle aligned icon'></i>
                  <div class='content'>
                    <a class='header' ng-click="viewPost(value.topic_id,true)">{{value.topic_subject}}</a>
                    <div class='description'>{{value.fullname}}</div>
                  </div>
                </div>
            </div>
          </div>
        </div>

      </div>
     
    </div>

    <div class="ui right dividing rail">
      <div class="ui mini celled list">
        <div  class="item "
              ng-repeat="(key, value) in topic.topic_to_user"
              style="margin: 7px; padding: 7px;"
              ng-class="{bg_white: value.on == '1'}" 
              title="{{value.see?value.see:'...'}}"
              ng-cloak
              >
          <img class="ui avatar image" ng-src="{{value.avatar}}" >
          <div class="content">
            <div class="header">{{value.nom}} {{value.pnom}}</div>
            {{value.depart}}
          </div>
          <i class="left floated green small check icon" ng-show="value.see"></i>
        </div>
      </div>
      <div class="ui search"></div>
    </div>

    <div class="ui red comments" ng-cloak>

      <h3 class="ui dividing header">{{topic.topic_subject}}</h3>

      <div class="comment disable" ng-repeat="(key, value) in topic.posts" ng-cloak>
        <a class="avatar">
          <img ng-src="{{value.avatar}}">
        </a>
        <div class="content">
          <div class="author">{{value.fullname}}</div>
          <div class="metadata">
            <span class="date">{{value.post_date}} #{{value.post_id}}</span>
          </div>
          <div btf-markdown="value.post_content" class="text pre_wrap">
          </div>
          <div class="actions">
            <a class="reply"></a>
          </div>
        </div>
      </div>

      <div>
        <div class="ui mini image label" ng-repeat="el in topicsSeeBy"> <img src="img/elliot.jpg"> {{el}} </div>
      </div>

      <div class="ui warning message" ng-show="(topic.topic_by_close && topic.topic_by_close!='n') || (topic.topic_to_close && topic.topic_to_close!='n')" ng-cloak>
        <div class="header">
        </div>
        <i class="icon info"></i>
        Cette notification et fermer 
       
        <span ng-if=" topic.topic_by_close!='n' || topic.topic_to_close!='n' "> par </span>
        <b ng-if="topic.topic_by_close!='n' ">{{topic.fullname}}</b>
        ,
        <b ng-if="topic.topic_to_close!='n' ">{{topic.topic_to_close_name}}</b>

      </div>

      <form class="ui reply form"
        ng-hide="(topic.iam_sender=='1' && topic.topic_by_close!='n') || (topic.iam_sender=='0' && topic.topic_to_close!='n')">

        <div class="field" ng-show="!topic" ng-cloak>
          <!--select class="ui dropdown fluid" ng-model="wpost.to">
            <option value='BAT'>BAT - BATIMENT</option>
            <option value='GC'>GC - GENIE CIVIL</option>
            <option value='DPI'>DPI - DÉPARTEMENT DE PRODUCTION ET INFRASTRUCTURE</option>
            <option value='CET'>C.E.T - CORPS D'ETAT TECHNIQUES</option>
            <option value='TC'>T.C - TECHNIQUO COMMERCIALE</option>
            <?php foreach ($fw->fetchAll("SELECT id_user, CONCAT(utilisateur.nom,' ',utilisateur.pnom) as fullname FROM utilisateur WHERE acl LIKE '%\"enable\":true%'") as $user){
              echo "<option value='$user->id_user'>$user->fullname</option>";
            } ?>
          </select-->




          <div class="ui fluid search selection dropdown">
            <input type="hidden" id="wpost_to">
            <i class="dropdown icon"></i>
            <div class="default text">Sélectionner une destination ...</div>
            <div class="menu">
              <div class="item" data-value="BAT">BAT - BATIMENT</div>
              <div class="item" data-value="GC">GC - GENIE CIVIL</div>
              <div class="item" data-value="DPI">DPI - DÉPARTEMENT DE PRODUCTION ET INFRASTRUCTURE</div>
              <div class="item" data-value="CET">C.E.T - CORPS D'ETAT TECHNIQUES</div>
              <div class="item" data-value="TC">T.C - TECHNIQUO COMMERCIALE</div>
                <?php foreach ($fw->fetchAll("SELECT id_user, CONCAT(utilisateur.nom,' ',utilisateur.pnom) as fullname, avatar, IF(date_act < NOW() - INTERVAL 30 SECOND, '', '<div class=\'green_led\'></div>') AS `on` FROM utilisateur WHERE acl LIKE '%\"enable\":true%'") as $user){
                  echo "<div class='item' data-value='$user->id_user'><img class='ui avatar image' src='$user->avatar' >$user->on $user->fullname</div>";
                } ?>

            </div>
          </div>


        </div>




        <div class="field" ng-show="!topic" ng-cloak>
          <div class="ui fluid input">
            <input type="text" placeholder="Objet..." ng-model="wpost.subject" maxlength="250">
          </div>
        </div>



        <div class="field">
          <textarea ng-model="wpost.content"></textarea>
          <p
            ng-show="markdown"
            btf-markdown="wpost.content"
            class="text pre_wrap"
            style="
              border: 1px dashed #ddd;
              margin: 20px 0;
              padding: 10px;
              color: #333;
              ">
          </p>

          <input  type="file"
                  id="file"
                  name="file"
                  ng-model="file"
                  onchange="angular.element(this).scope().upload(this)"
                  style="visibility: hidden;" 
                  >

          <!--div style="text-align: right;" ng-show="markdown"><a href="https://www.disko.fr/reflexions/technique/markdown-lhtml-pour-le-monde/" target="_blank">Tutoriel MARKDOWN</a></div-->
        </div>

        <label class="ui icon green button" tabindex="0" for="file">
            <i class="paperclip icon" ></i>
        </label>

        <div class="ui blue labeled submit icon button" ng-click="sendPost()" ng-class="{disabled: !wpost.content }">
          <i class="icon paper plane"></i> Envoyer
        </div>
        <div  class="ui labeled submit icon button"
              ng-show="
                (topic.iam_sender=='1' && topic.topic_by_close=='n') || (topic.iam_sender=='0' && topic.topic_to_close=='n')
              "
              ng-click="closePost()">
          <i class="icon quote right"></i> Clôturer la conversation
        </div>

        <img
          class="ui mini image right floated"
          src="img/markdown-mark.svg"
          style="cursor: pointer;" ng-click="markdown=!markdown"
          title="Markdown text format"
          >

        
      </form>

      <!--div class="ui divider"></div>

      <div class="ui mini horizontal divided list">
        <div class="item" ng-repeat="(key, value) in topic.topic_to_user" ng-cloak>
          <img class="ui avatar image" ng-src="{{value.avatar}}">
          <div class="content">
            <div class="header">{{value.nom}} {{value.pnom}}</div>
          </div>
        </div>
      </div-->

    </div>

  </div>

</div>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $interval, $filter, $http) {
  
  $scope.loadTopics = function(clear){
    $http.get('api/?topics')
      .then(function(res){
        $scope.topics = res.data;
        //console.log("topics list", $scope.topics);
        
        <?php
        if (isset($_GET['id']) && $_GET['id']!="" )
          echo "if (!\$scope.v){\$scope.viewPost('$_GET[id]',true);\$scope.v=true;}";
        ?>

        if ($scope.topic){
          $scope.viewPost($scope.topic.topic_id,clear);
          //$scope.$apply();
        }else{
          $scope.newPost(clear);
        }
      });
  };


  $scope.loadSee = function(clear){
    console.log('.');
    
    if ($scope.topic.topic_id){
      $http.post('api/?topics&see',{
        topic_id: $scope.topic.topic_id
      })
      .then(function(r){
        $scope.topic.topic_see = r.data;
      });
    }
  };

  $scope.sendPost = function(){
    $scope.wpost.to = $('#wpost_to').val();
    if ((($scope.wpost.to && $scope.wpost.subject)||$scope.wpost.topic_id) && $scope.wpost.content){
      $scope.loading = "loaging";
      $http.post('api/?topics',$scope.wpost)
        .then(function(r){
          console.log(r);
          if (r.data.sqlState == "00000" && r.data.id){
            $scope.loadTopics(true);
            //$scope.$apply();
          }
        })
      ;
    }
  }

  $scope.viewPost = function(topic_id,clear){
    //console.log(topic_id);

    $scope.topic = $scope.topics.find(line => line.topic_id === topic_id );
    //console.log('test',$scope.topics);
    //console.log('test',$scope.topic);
    if (clear){
      if ($scope.topic === undefined)
      {
        $scope.topic = {topic_id:null};
      }
      
      $scope.wpost  = {
        topic_id: $scope.topic.topic_id,
        content: null
      };
    }

    $scope.loading = "";

    $scope.loadSee();
    
    //console.log('viewPost',$scope.topic);
  }

  $scope.closePost = function(topic_id){
    $http.post('api/?topics&close', {
      post_content:   $scope.wpost.content,
      topic_id:       $scope.topic.topic_id,
      topic_by:       $scope.topic.topic_by,
      topic_to:       $scope.topic.topic_to,
      topic_by_close: $scope.topic.topic_by_close,
      topic_to_close: $scope.topic.topic_to_close
    })
      .then(function(r){
        $scope.loadTopics();
      })
    ;
  }

  $scope.newPost = function(clear){
    if (clear){
      $scope.topic = null;
      $scope.wpost  = null;
    }
    $('#wpost_to').dropdown('restore defaults');
  }

  $scope.upload = function(el){
    var fileData = new FormData();
    fileData.append('file', el.files[0]);
    console.log('el',el.files[0].name);
    
    //console.log(fileData);
    $http.post('api/?upload=repo&user',fileData, {
      headers: {
        'Content-Type': undefined
      },
      transformResponse: angular.identity
    })
    .then(function(r){
      let res = JSON.parse(r.data);

      console.log('ext', res.extension);
      // add fullpath to message
      let ext = res.extension;

      if (!$scope.wpost)
        $scope.wpost  = {
          topic_id: null,
          content: null
        };
      if (!$scope.wpost.content)
        $scope.wpost.content = "";
      
      $scope.wpost.content += '  \r';
      if ( ext=='png'||ext=='jpg'||ext=='jpeg'||ext=='svg' )
        $scope.wpost.content += '!';
      $scope.wpost.content += '['+el.files[0].name+']('+res.fullpath+')';

    });
  }

  $scope.loadTopics();
  
  $interval(function(){
    if (!document.hidden)


    if (typeof document.hidden !== "undefined" || typeof document.mozHidden !== "undefined" || typeof document.msHidden !== "undefined" || typeof document.webkitHidden !== "undefined")
    {
      $scope.loadTopics();
      //$scope.loadSee();
    }
    /*$http.get('api/?topics')
      .then(function(res){
        $scope.topics = res.data;
        console.log("topics list", $scope.topics);
      });*/
  },2000,0);

});</script>

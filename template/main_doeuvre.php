<?php
  if(!isallow("admin") && !isallow("cet") && !isallow("tc") ) {
    echo "<script type='text/javascript'>location='?p=err&err=accès non autoriser&msg=Vous n\'avez pas l\'autorisation d\'accéder a cette rubrique, contactez l\'administrateur';</script>";
    exit;
  }
?>


<div ng-controller="TodoCtrl" >
  <h2>MAIN D'OEUVRE</h2>

  <div class="ui action input">
    <input type="text" placeholder="Recherche ..." ng-model="srh_mo">
    <button class="ui icon button">
      <i class="search icon"></i>
    </button>
  </div>

  <table class="ui compact selectable red table">
    <thead>
      <tr>
        <th width="5%">Code</th>
        <th width="85%">Libelle poste</th>
        <th width="10%" class="right aligned">Cout horaire</th>
        <th>
        </th>
        <th>
          <div class='ui mini red icon button'
            data-tooltip="Ajouter MAIN D'OEUVRE"
            data-inverted=""
            data-position="left center"
            ng-click="detail();"
            >
            <i class='add icon'></i>
          </div>          
        </th>
      </tr>
    </thead>
    <tbody>
      <tr
        ng-repeat="l_mo in main_oeuvre | filter:srh_mo"
        ng-cloak
        >
        <td>{{l_mo.code_poste}}</td>
        <td>{{l_mo.libelle}}</td>
        <td class="right aligned">{{l_mo.cout_horaire | number:2}}</td>
        <td class="right aligned">
          <div
            class="ui mini basic icon button"
            data-tooltip="Supprimer {{l_mo.code_poste}}"
            data-inverted=""
            data-position="left center"
            ng-click="delete(this.l_mo);"
            >
            <i class='trash icon'></i>
          </div>
        </td>
        <td class="right aligned">
          <div
            class="ui mini basic icon button"
            data-tooltip="Modifier {{l_mo.code_poste}}"
            data-inverted=""
            data-position="left center"
            ng-click="detail(this.l_mo);"
            >
            <i class='write icon'></i>
          </div>
        </td>
      </tr>
    </tbody>
  </table>

  <!-- Ajouter / Modifier Main oeuvre -->
  <form method="POST" action="" autocomplete="off">
    <div class="ui modal Main_oeuvre">
      <div class="header">
        {{comp_mo.titel}} MAIN D'OEUVRE
      </div>

      <div class="ui form scrolling content">
        <div class="fields">
          <div class="three wide field" ng-class="{'error': !comp_mo.code_poste }">
            <label>Code</label>
            <input type="text" ng-model="comp_mo.code_poste" class="uppercase">
          </div>
          <div class="ten wide field" ng-class="{'error': !comp_mo.libelle }">
            <label>Libelle poste</label>
            <input type="text"  ng-model="comp_mo.libelle">
          </div>
          <div class="three wide field" ng-class="{'error': !comp_mo.cout_horaire }">
            <label>Cout horaire</label>
            <input type="number" toNumber ng-model="comp_mo.cout_horaire" min="0" step="0.01">
          </div>
        </div>
      </div>

      <div class="actions">
        <div
          class="ui deny icon button"
          ng-show="comp_mo.titel=='Modifier'"
          ng-click="delete(comp_mo)"
          ><i class="trash icon"></i>
        </div>
        <div class="ui deny button">
          Annuler
        </div>
        <div
          class="ui green right button"
          ng-click="update(comp_mo)"
          ng-disabled=""
          ng-class="{ 'disabled' : !comp_mo.code_poste || !comp_mo.libelle || !comp_mo.cout_horaire }"
          >{{comp_mo.titel}}
        </div>
      </div>
    </div>
  </form>

</div>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $http) {
    
  $http.get('api/?list=main_doeuvre')
    .then(function(res){
      angular.forEach(res.data, function(ele) {
        ele.cout_horaire = parseFloat(ele.cout_horaire);
      });
      $scope.main_oeuvre = res.data;
      console.log($scope.main_oeuvre);
    });
  
  $scope.detail = function(mo){
    console.log('detail',mo);
    if (mo)
    {
      $scope.comp_mo = angular.copy(mo) ;
      $scope.comp_mo.titel = "Modifier";
    }
    else
      $scope.comp_mo = {titel:"Ajouter",code_poste:null,libelle:null,cout_horaire:0.00};
    $('.ui.modal.Main_oeuvre').modal('show');
  }

  $scope.update = function(mo){
    console.log('update',mo);
    $http.post('api/?update=main_doeuvre',mo)
    .then(function(r){
      console.log('result',r);
      if (r.data.res = "done!" && r.data.pdo.sqlState == "00000")
        location.reload();
    });
  }

  $scope.delete = function(mo){
    console.log('delete',mo);
    if (confirm("Etre vous sure de vouloir le supprimer"))
    {
      $http.post('api/?delete=main_doeuvre&w=code_poste&e='+mo.code_poste,{})
      .then(function(r){
        console.log('result',r);
        if (r.data.res = "done!" && r.data.pdo.sqlState == "00000")
          location.reload();
      });
    }
  }
}); </script>
<div ng-controller="TodoCtrl" >
  
  <table class="ui red selectable link compact table" ng-cloak>
    <thead>
      <tr>
        <th colspan="6">
            <h2>LISTE DES CLIENTS</h2>
        </th>
        <th class="right aligned">
          <div class="ui mini icon input">
            <input type="text" placeholder="Recherche ..." ng-model="c_tache" >
            <i class="search icon"></i>
          </div>
          <a href="?p=client/add">
            <div class='ui mini red icon button'
              data-tooltip="Ajouter un CLIENT"
              data-variation="mini"
              data-inverted=""
              data-position="left center"
              >
              <i class='add icon'></i>
            </div>
          </a>
        </th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="ele in client | filter:c_tache">
        <td>{{ele.num_client}}</td>
        <td>{{ele.nom_client}}</td>
        <td>{{ele.responseble}}</td>
        <td>{{ele.tel}}</td>
        <td>{{ele.email}}</td>
        <td>{{ele.adresse}}</td>
        <td class="right aligned">
          <a
            class="ui mini basic icon button"
            data-tooltip="Modifier {{ele.nom_client}}"
            data-inverted=""
            data-position="left center"
            ng-click="detail(this.num_client);"
            href="?p=client/add&num_client={{ele.num_client}}"
            >
            <i class='write icon'></i>
          </a>

        </td>
      </tr>
    </tbody>
  </table>

</div>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $http) {

    $http.get('api/?list=client')
      .then(function(res){
        $scope.client = res.data;
        console.log("client", $scope.client);
      });

}); </script>
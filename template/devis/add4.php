<?php // ESTIMATION DU DEVIS

$num_sub_devis = sql_inj($_GET['projet'],'');
list($num_devis,$diff) = explode('-', $num_sub_devis.'-');

if (!$num_devis){
  $_GET['err']='Erreur 404';
  $_GET['msg']='Page not found';
  include("template/err.php");
  die();
}

?>

<?=HEADER_PAGE?>

<div class="ui page dimmer print_ignore">
  <div class="content">
    <h2 class="ui inverted icon header">
      <div class="ui text loader"></div>
    </h2>
  </div>
</div>


<div ng-controller="TodoCtrl" ng-cloak>

  <?php if (isallow("programmer")) echo DEBUG_BUTTON;?>

  <div class="ui fixed bottom sticky print_ignore">
    <div class="ui image label" >
      <i class="hotjar icon"></i>
      {{sub_devis.num_devis || devis.num_devis}}
      <div class="detail">{{sub_devis.obj_devis || devis.obj_devis}}</div>
    </div>
  </div>

  <div class="ui attached small steps print_ignore">
    <a class="step" href="?p=devis/add1&projet=<?=$num_sub_devis;?>">
      <i class="id card icon"></i>
      <div class="content">
        <div class="title">Projet</div>
        <div class="description">Informations du Projet</div>
      </div>
    </a>
    <div class="ui step top left pointing dropdown">
      <i class="file excel icon"></i>
      <div class="content">
        <div class="title">Devis Quantitatif EXCEL</div>
        <div class="description">Analyse du Fichier Client</div>
      </div>
      <div class="menu">
        <a class="item" ng-repeat="ele in devis.sub_devis" href="?p=devis/add2&projet={{ele.num_devis}}">
        <i class="icon file"></i>
        {{ele.obj_devis}}</a>
      </div>
    </div>
    <a class="step" href="?p=devis/add3&projet=<?=$num_sub_devis;?>">
      <i class="info icon"></i>
      <div class="content">
        <div class="title">Définition des frais</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="step"  href="?p=devis/add5&projet=<?=$num_sub_devis?>"
       ng-class="{disabled:!sub_devis.dv_etudes}">
      <i class="codepen icon"></i>
      <div class="content">
        <div class="title">ETUDES</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="active red_border step" href="?p=devis/add4&projet=<?=$num_sub_devis;?>">
      <i class="calculator icon"></i>
      <div class="content">
        <div class="title">Estimation du DEVIS</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="step" href="?p=devis/add6&projet=<?=$num_sub_devis;?>">
      <i class="map icon"></i>
      <div class="content">
        <div class="title">B.P.U</div>
        <div class="description"></div>
      </div>
    </a>
  </div>

<!-- <div class="ui red icon button print_ignore" ng-click="dbg()"><i class="code icon"></i></div> -->



<div class="ui basic segment" ng-repeat="sdv in devis.sub_devis" ng-init="$last && save();" ng-cloak>
  
  <!--h4 class="ui horizontal grey divider header print_ignore" ng-cloak>
    <i class="tag icon"></i>
    {{sdv.num_devis}} / {{sdv.nom_devis}}
  </h4-->

  <div class="ui clearing inverted segment print_ignore">
      
    <h3 class="ui left floated header">
      {{sdv.num_devis}} / {{sdv.nom_devis}}
    </h3>

    <div    class="ui right floated inverted button"
            ng-click="printElement('#dv_'+$id)">
            <i class="print icon"></i> IMPRIMER
    </div>
    <div    class="ui dropdown right floated inverted olive button"
            ng-click="exportXls(this)">
            <i class="file excel outline icon"></i> Exporter vers EXCEL
    </div>
    <div    class="ui top left pointing dropdown right floated inverted olive button"
            ng-click="showRabaisModel(this)">
            <i class="resolving icon"></i> Rabais
    </div>

  </div>

<!-- 

oooooooooo.                          o8o              ooooooooooooo            .o8       oooo
`888'   `Y8b                         `"'              8'   888   `8           "888       `888
 888      888  .ooooo.  oooo    ooo oooo   .oooo.o         888       .oooo.    888oooo.   888   .ooooo.
 888      888 d88' `88b  `88.  .8'  `888  d88(  "8         888      `P  )88b   d88' `88b  888  d88' `88b
 888      888 888ooo888   `88..8'    888  `"Y88b.          888       .oP"888   888   888  888  888ooo888
 888     d88' 888    .o    `888'     888  o.  )88b         888      d8(  888   888   888  888  888    .o
o888bood8P'   `Y8bod8P'     `8'     o888o 8""888P'        o888o     `Y888""8o  `Y8bod8P' o888o `Y8bod8P'

 -->




    <table  class='ui striped selectable compact celled table print_ignore' 
            ng-init="sdv.total_devis=0" 
            id="dv_{{::$id}}"
            ng-cloak>

      <thead>
        <tr class="print_only">
          <th colspan="8">

            <!--button class="ui print_ignore right floated mini icon button"
                    data-inverted=""
                    data-position="bottom right"
                    data-tooltip="Exporter les donner ver fichier Excel du client"
                    ng-click="exportXls(this)"><i class="file excel outline icon"></i> Exporter vers EXCEL
            </button>

            <button class="ui print_ignore right floated mini icon button"
                    data-inverted=""
                    data-position="bottom right"
                    data-tooltip="Imprimer ..."
                    ng-click="printElement('#dv_'+$id)"><i class="print icon"></i> IMPRIMER
            </button-->

            <h2>
              {{sdv.num_devis}} / {{sdv.obj_devis}}
            </h2>
            
          </th>
        </tr>
        <tr>
          <th class="right left   ">N</th>
          <th class="right left   " width='50%'>Designation</th>
          <th class="right center ">Um</th>
          <th class="right aligned">Qte</th>
          <th class="right aligned">Prix U sec</th>
          <th class="right aligned">KV</th>
          <th class="right aligned">PV</th>
          <th class="right aligned">Montant</th>
          <th class="right aligned">Rabais %</th>
          <?php if (isallow("programmer")) echo '<th></th>'; ?>
          
        </tr>
      </thead>
      <tbody>
            <!-- ng-init="reformatLine(ele)" -->
        <tr ng-repeat="ele in sdv.xls" 
            ng-if="ele.matching != 'IGNORER'"
            ng-class="{
              negative:   (!ele.nArticle && !ele.matching),

              yellow_bg:    ele.matching == 'NOTIFICATION LOT' ||
                            ele.matching == 'NOTIFICATION TACHE',

              warning:      ele.matching == 'LOT' ||
                            ele.matching == 'SOUS LOT' ||
                            ele.matching == 'IGNORER',

              positive:     ele.matching == 'TOTAL LOT' ||
                            ele.matching == 'TOTAL SOUS LOT' ||
                            ele.matching == 'TOTAL REALISATION HT',

              red_bg:       ele.matching == null ||
                            ele.matching == '',

              print_ignore: ele.matching == 'IGNORER'
            }"
            ng-dblclick="detaille(ele)"
            ng-cloak>
          <td>{{ele.nArticle}}</td>
          <td>{{ele.designation}}</td>
          <td>{{ele.um_cos}}</td>
          <td class="right aligned">{{ele.qte            | number:2}}</td>
          <td class="right aligned">{{ele.deb_sec        | number:2}}</td>
          <td class="right aligned">{{ele.kv             | number:2}}</td>
          <td class="right aligned">{{ele.deb_sec*ele.kv | number:0
          }}</td>
          <td class="right aligned">
            <element ng-if="ele.um_cos == 'M3'">{{ele.mnt | number:3}}</element>
            <element ng-if="ele.um_cos != 'M3'">{{ele.mnt | number:2}}</element>
          </td>
          <td class="center aligned">{{ele.rabais}}</td>
          <?php if (isallow("programmer")) echo '<td class="right aligned active">{{ele.kv_lot}}</td>'; ?>
          
        </tr>
        <!--tr>
          <td>TOTAL</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td>{{sdv.total_mnt | number:2}}</td>
          <td></td>
          <?php if (isallow("programmer")) echo '<td></td>'; ?>
          
        </tr-->
        
      <tbody>
    </table>


<!-- 

      .o.                             oooo                                    oooooooooooo
     .888.                            `888                                    `888'     `8
    .8"888.     ooo. .oo.    .oooo.    888  oooo    ooo  .oooo.o  .ooooo.      888
   .8' `888.    `888P"Y88b  `P  )88b   888   `88.  .8'  d88(  "8 d88' `88b     888oooo8
  .88ooo8888.    888   888   .oP"888   888    `88..8'   `"Y88b.  888ooo888     888    "
 .8'     `888.   888   888  d8(  888   888     `888'    o.  )88b 888    .o     888
o88o     o8888o o888o o888o `Y888""8o o888o     .8'     8""888P' `Y8bod8P'    o888o
                                            .o..P'
                                            `Y8P'

 -->


  <div class="ui raised very padded container piled segment print_ignore" id="analyse_{{::$id}}" ng-cloak>

    <button class="ui print_ignore right floated mini icon button" style="z-index: 1;"
            data-inverted=""
            data-position="bottom right"
            data-tooltip="Imprimer avec page en tete"
            ng-click="printElement('#analyse_'+$id,1)"><i class="print icon"></i> IMPRIMER AVEC PAGE ENTETE
    </button>

    <button class="ui print_ignore right floated mini icon button" style="z-index: 1;" 
            data-inverted=""
            data-position="bottom right"
            data-tooltip="Imprimer ..."
            ng-click="printElement('#analyse_'+$id)"><i class="print icon"></i>  IMPRIMER
    </button>


    <div class="ui basic segment">
      PROJET : <b>{{sdv.num_devis}} / {{sdv.nom_devis}}</b> <br>
      Objet : <b>{{sdv.obj_devis}}</b> <br>
    </div>
    <div class="ui center aligned basic segment">
      <h1>ANALYSE FINANCIERE</h1>
    </div>

    <table class="ui striped celled table" ng-init="analyse_f(sdv)" ng-cloak>
      <thead>
        <tr>
          <th class="left aligned">DESIGNATION</th>
          <th class="right aligned">MONTANT</th>
          <th class="right aligned">
            {{sdv.nbr_logs>0 ? 'PRIX / LOGTS' : 'PRIX DU M2'}}
          </th>
          <th class="center aligned">%</th>
        </tr>
      </thead>


        <tr ng-repeat="ele in sdv.lot">
          <td class="left aligned"><h4>{{ele.des}}</h4></td>
          <td class="right aligned"><h4>{{ ele.mnt | number:2 }}</h4></td>
          <td class="right aligned"><h4>{{ ele.mnt / sdv.surfLogts | number:2 }}</h4></td>
          <td class="center aligned"><h4>{{ (ele.mnt * 100) / sdv.mnt_rl | number:2 }}</h4></td>
        </tr>

        <tr>
          <td class="left aligned under_line"><h4>INSTALLATION CHANTIER</h4></td>
          <td class="right aligned under_line"><h4>{{ sdv.installation_CH || 0 | number:2 }}</h4></td>
          <td class="right aligned under_line"></td>
          <td class="center aligned under_line"><h4>{{ (sdv.installation_CH * 100) / sdv.mnt_rl | number:2 }}</h4></td>
        </tr>

        <tr>
          <td class="left aligned"><h4>MONTANT RÉALISATION EN HT</h4></td>
          <td class="right aligned"><h4>{{ sdv.mnt_rl | number:2}}</h4></td>
          <td class="right aligned"><h4>{{ sdv.mnt_rl / sdv.surfLogts | number:2}}</h4></td>
          <td class="center aligned"><h4>100,00</h4></td>
        </tr>

        <tr ng-if="sdv.rabais.type==1">
          <td class="left aligned under_line"><h4>RABAIS COMMERCIAL ({{ sdv.rabais.commercial }}%)</h4></td>
          <td class="right aligned under_line"><h4>{{ sdv.mnt_rb | number:2}}</h4></td>
          <td class="right aligned under_line"></td>
          <td class="center aligned under_line"></td>
        </tr>
        <tr ng-if="sdv.rabais.type==1">
          <td class="left aligned"><h4>MONTANT RÉALISATION EN HT APRES RABAIS</h4></td>
          <td class="right aligned"><h4>{{ sdv.mnt_rl_rb | number:2}}</h4></td>
          <td class="right aligned"><h4>{{ sdv.mnt_rl_rb / sdv.surfLogts | number:2}}</h4></td>
          <td class="center aligned"></td>
        </tr>

        <tr>
          <td class="left aligned"><h4>MONTANT TVA ({{sdv.tva}}%)</h4></td>
          <td class="right aligned"><h4>{{ sdv.mnt_tva | number:2}}</h4></td>
          <td class="right aligned"><h4>{{ sdv.mnt_tva / sdv.surfLogts  | number:2}}</h4></td>
          <td class="center aligned"></td>
        </tr>

        <tr>
          <td class="left aligned under_line">
            <h4>
              MONTANT RÉALISATION EN TTC 
              <span ng-if="sdv.rabais.type==1">APRES RABAIS</span>
            </h4>
          </td>
          <td class="right aligned under_line"><h4>{{ sdv.mnt_ttc_rb | number:2}}</h4></td>
          <td class="right aligned under_line"><h4>{{ sdv.mnt_ttc_rb / sdv.surfLogts | number:2}}</h4></td>
          <td class="center aligned under_line"></td>
        </tr>

        <tr ng-if="sdv.dv_etudes">
          <td class="left aligned"><h4>MONTANT ETUDES EN HT</h4></td>
          <td class="right aligned"><h4>{{ sdv.etudes.mnt | number:2}}</h4></td>
          <td class="right aligned"><h4>{{ sdv.etudes.mnt / sdv.surfLogts | number:2}}</h4></td>
          <td class="center aligned"><h4>{{ sdv.etudes.taux | number:2 }}</h4></td>
        </tr>
        <tr ng-if="sdv.dv_etudes">
          <td class="left aligned"><h4>MONTANT TVA ({{sdv.etudes.tva}}%) ETUDES</h4></td>
          <td class="right aligned"><h4>{{ sdv.etudes.mnt_tva | number:2}}</h4></td>
          <td class="right aligned"><h4>{{ sdv.etudes.mnt_tva / sdv.surfLogts | number:2}}</h4></td>
          <td class="center aligned"></td>
        </tr>
        <tr ng-if="sdv.dv_etudes">
          <td class="left aligned under_line"><h4>MONTANT ETUDES EN TTC</h4></td>
          <td class="right aligned under_line"><h4>{{ sdv.etudes.mnt_ttc | number:2}}</h4></td>
          <td class="right aligned under_line"><h4>{{ sdv.etudes.mnt_ttc /sdv.surfLogts | number:2}}</h4></td>
          <td class="right aligned under_line"></td>
        </tr>

        <tr ng-if="sdv.dv_etudes">
          <td class="left aligned"><h4>MONTANT ETUDES ET RÉALISATION EN HT</h4></td>
          <td class="right aligned"><h4>{{ sdv.mnt_et_rl | number:2}}</h4></td>
          <td class="right aligned"><h4>{{ sdv.mnt_et_rl / sdv.surfLogts | number:2}}</h4></td>
          <td class="center aligned"></td>
        </tr>
        <tr ng-if="sdv.dv_etudes">
          <td class="left aligned"><h4>MONTANT TVA ETUDES ET RÉALISATION</h4></td>
          <td class="right aligned"><h4>{{ sdv.mnt_tva_et_rl | number:2}}</h4></td>
          <td class="right aligned"><h4>{{ sdv.mnt_tva_et_rl / sdv.surfLogts | number:2}}</h4></td>
          <td class="center aligned"></td>
        </tr>
        <tr ng-if="sdv.dv_etudes">
          <td class="left aligned under_line"><h4>MONTANT ETUDES ET RÉALISATION EN TTC</h4></td>
          <td class="right aligned under_line"><h4>{{ sdv.mnt_ttc_et_rl | number:2}}</h4></td>
          <td class="right aligned under_line"><h4>{{ sdv.mnt_ttc_et_rl / sdv.surfLogts | number:2}}</h4></td>
          <td class="center aligned under_line"></td>
        </tr>

        <tr>
          <td class="left aligned"><h4>{{sdv.nbl_devis ? 'NOMBRE DE LOGEMENTS' : 'TOTAL SURFACE HABITABLE / CONSTRUITE'}}</h4></td>
          <td class="right aligned"><h4>{{ sdv.surfLogts }}</h4></td>
          <td class="right aligned"></td>
          <td class="center aligned"></td>
        </tr>

    </table>

  </div>


<!-- 

ooooooooo.                                                 .oooooo.
`888   `Y88.                                              d8P'  `Y8b
 888   .d88'  .ooooo.   .ooooo.   .oooo.   oo.ooooo.     888
 888ooo88P'  d88' `88b d88' `"Y8 `P  )88b   888' `88b    888
 888`88b.    888ooo888 888        .oP"888   888   888    888     ooooo
 888  `88b.  888    .o 888   .o8 d8(  888   888   888    `88.    .88'
o888o  o888o `Y8bod8P' `Y8bod8P' `Y888""8o  888bod8P'     `Y8bood8P'
                                            888
                                           o888o

 -->


<div class="ui raised very padded container piled segment print_ignore" id="recapG_{{::$id}}" ng-cloak>

    <button class="ui print_ignore right floated mini icon button" style="z-index: 1;"
            data-inverted=""
            data-position="bottom right"
            data-tooltip="Imprimer avec page en tete"
            ng-click="printElement('#recapG_'+$id,1)"><i class="print icon"></i> IMPRIMER AVEC PAGE ENTETE
    </button>

    <button class="ui print_ignore right floated mini icon button" style="z-index: 1;" 
            data-inverted=""
            data-position="bottom right"
            data-tooltip="Imprimer ..."
            ng-click="printElement('#recapG_'+$id)"><i class="print icon"></i>  IMPRIMER
    </button>


    <div class="ui basic segment">
      PROJET : <b>{{sdv.num_devis}} / {{sdv.nom_devis}}</b> <br>
      Objet : <b>{{sdv.obj_devis}}</b> <br>
    </div>
    <div class="ui center aligned basic segment">
      <h1>RECAPITULATION GENERALE</h1>
    </div>


    <table class="ui striped celled table" ng-init="analyse_f(sdv)" ng-cloak>
      <thead>
        <tr>
          <th class="left   aligned">DESIGNATION</th>
          <th class="right aligned">MONTANT</th>
        </tr>
      </thead>


        <tr ng-if="sdv.dv_etudes">
          <td><h4>Etudes</h4></td>
          <td style="text-align: right;"><h4>{{ sdv.etudes.mnt | number:2 }}</h4></td>
        </tr>

        <tr ng-if="sdv.dv_etudes">
          <td class="left aligned"><h4>MONTANT ETUDES HT</h4></td>
          <td class="right aligned"><h4>{{ sdv.etudes.mnt | number:2}}</h4></td>
        </tr>
        <tr ng-if="sdv.dv_etudes">
          <td class="left aligned"><h4>TVA ({{sdv.tva_etudes}}%) ETUDES</h4></td>
          <td class="right aligned"><h4>{{ sdv.etudes.tva_mnt = sdv.etudes.mnt * sdv.tva_etudes/100 | number:2}}</h4></td>
        </tr>
        <tr ng-if="sdv.dv_etudes">
          <td class="left aligned"><h4>MONTANT ETUDES TTC</h4></td>
          <td class="right aligned"><h4>{{ sdv.etudes.mnt + sdv.etudes.tva_mnt | number:2}}</h4></td>
        </tr>

        <tr ng-if="sdv.installation_CH">
          <td><h4>Installation Chantier</h4></td>
          <td style="text-align: right;"><h4>{{ sdv.installation_CH | number:2 }}</h4></td>
        </tr>

        <tr ng-repeat="ele in sdv.lot">
          <td ><h4>{{ele.des}}<h4></td>
          <td style="text-align: right;"><h4>{{ ele.mnt | number:2 }}</h4></td>
        </tr>

        <tr>
          <td class="left aligned"><h4>MONTANT TOTAL EN HT</h4></td>
          <td class="right aligned"><h4>{{ sdv.total_mnt | number:2}}</h4></td>
        </tr>        
        <tr>
          <td class="left aligned"><h4>TVA ({{sdv.tva}}%)</h4></td>
          <td class="right aligned"><h4>{{ (sdv.total_mnt * sdv.tva / 100) | number:2}}</h4></td>
        </tr>
        <tr>
          <td class="left aligned"><h4>MONTANT TOTAL EN TTC</h4></td>
          <td class="right aligned"><h4>{{ (sdv.total_mnt + (sdv.total_mnt * sdv.tva / 100)) | number:2}}</h4></td>
        </tr>

        <tr ng-if="sdv.dv_etudes">
          <td class="left aligned"><h4>MONTANT ETUDES ET RÉALISATION HT</h4></td>
          <td class="right aligned"><h4>{{ sdv.total_mnt + sdv.etudes.mnt | number:2}}</h4></td>
        </tr>
        <tr ng-if="sdv.dv_etudes">
          <td class="left aligned"><h4>TVA ETUDES ET RÉALISATION</h4></td>
          <td class="right aligned"><h4>{{ sdv.mnt_tva_et_rl | number:2}}</h4></td>
          <!--    sdv.tva_mnt + sdv.etudes.tva_mnt -->
        </tr>
        <tr ng-if="sdv.dv_etudes">
          <td class="left aligned"><h4>MONTANT ETUDES ET RÉALISATION TTC</h4></td>
          <td class="right aligned"><h4>{{ sdv.total_mnt + sdv.etudes.mnt + sdv.tva_mnt + sdv.etudes.tva_mnt | number:2}}</h4></td>
        </tr>

    </table>


    <div class="ui basic segment">
      Arrêter le présent marché à la somme (toutes taxes comprises) de :<br>
      <div class="ui center aligned basic segment">
        <h4 class="uppercase">{{NumberToLetter( sdv.mnt_ttc_et_rl || sdv.mnt_ttc_rb )}}</h4>
      </div>
      
    </div>


  </div>

</div>


<!-- 

 .oooooo..o                                   oooooooooo.                 .              o8o  oooo
d8P'    `Y8                                   `888'   `Y8b              .o8              `"'  `888
Y88bo.       .ooooo.  oooo  oooo   .oooo.o     888      888  .ooooo.  .o888oo  .oooo.   oooo   888
 `"Y8888o.  d88' `88b `888  `888  d88(  "8     888      888 d88' `88b   888   `P  )88b  `888   888
     `"Y88b 888   888  888   888  `"Y88b.      888      888 888ooo888   888    .oP"888   888   888
oo     .d8P 888   888  888   888  o.  )88b     888     d88' 888    .o   888 . d8(  888   888   888
8""88888P'  `Y8bod8P'  `V88V"V8P' 8""888P'    o888bood8P'   `Y8bod8P'   "888" `Y888""8o o888o o888o

 -->

<!-- 
    .                       oooo
  .o8                       `888
.o888oo  .oooo.    .ooooo.   888 .oo.    .ooooo.
  888   `P  )88b  d88' `"Y8  888P"Y88b  d88' `88b
  888    .oP"888  888        888   888  888ooo888
  888 . d8(  888  888   .o8  888   888  888    .o
  "888" `Y888""8o `Y8bod8P' o888o o888o `Y8bod8P'

 -->

  <div class="ui modal detaille " id="detaille" style="z-index: 2">
    <i class="close icon print_ignore"></i>
    <div class="content basic padded segment">
      
      <form autocomplete="off" class="ui form" id="my-section">

        <h2 class="header">{{th.tache_client}}</h2>
        <div class="equal width fields">
          <div class="field">
            <label>Nom Lot</label>
            <input type="text" value="{{th.nom_lot}}" readonly>
          </div>
          <div class="field">
            <label>Nom sous Lot</label>
            <input type="text"  value="{{th.slot.nom_slot}}" readonly>
          </div>
        </div>

        <hr class="style3 print_ignore"></hr>

        <div class="equal width fields">
          <div class="three wide field">
            <label>Code</label>
            <input type="text" ng-model="th.slot.tache.num_tache" class="uppercase" readonly>
          </div>
          <div class="field">
            <label>Nom du sous detail</label>
            <input type="text" value="{{th.slot.tache.nom_tache}}" readonly>
          </div>
        </div>
        <div class="equal width fields">
          <div class="field">
            <label>Unite de mesure</label>
            <input type="text" value="{{th.slot.tache.um}}" readonly>
          </div>

          <div class="field">
            <label>Temps Unitaire</label>
            <input type="text" value="{{th.slot.tache.tmp_u}}" readonly>
          </div>
          <div class="field">
            <label>Quantité</label>
            <input type="text" value="{{th.slot.tache.qte}}" readonly>
          </div>
          <div class="field">
            <label>Volume Horaire</label>
            <input type="text" value="{{th.slot.tache.vol_horaire}}" readonly>
          </div>
          <div class="field">
            <label>Prix Unitaire</label>
            <input type="text" value="{{th.slot.tache.prx_unitaire}}" readonly>
          </div>
        </div>
        <div class="field">
          <label>Observation</label>
          <textarea rows="2" readonly>{{th.slot.tache.observation}}</textarea>
        </div>
        <div class="field">
          <label>Descriptif</label>
          <textarea rows="2" readonly>{{th.slot.tache.descriptif}}</textarea>
        </div>

        <hr class="style3"></hr>
        
        <div class="no_break">
          <h4><i class='circular inverted users icon'></i> Main D'oeuvre</h4>
          
          <table  class="ui yellow selectable very compact table">
            <thead>
              <tr>
                <th width="">Code</th>
                <th width="100%">Qualification</th>
                <th width="" class="center aligned">Nombre</th>
                <th width="" class="right aligned">Amplitude</th>
                <th width="" class="right aligned">Cout horaire</th>
                <th width="" class="right aligned">Montant</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat=" line_mo in th.slot.tache.main_doeuvre">
                <td>{{line_mo.code_poste}}</td>
                <td>{{line_mo.libelle}}</td>
                <td class="center aligned">{{line_mo.nombre | number:0}}</td>
                <td class="right aligned">{{line_mo.amplitude | number:0}}</td>
                <td class="right aligned">{{line_mo.cout_horaire | number:2}}</td>
                <td class="right aligned">{{line_mo.montant | number:2}}</td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <th colspan="4" class="right aligned">
                  Volume horaire de l'equipe: <b>{{th.slot.tache.vol_h_mo | number:0}}</b><br>
                  Prix horaire de l'equipe: <b>{{th.slot.tache.prx_h_mo | number:2}}</b><br>
                  Cout de la main d'oeuvre: <b>{{th.slot.tache.cout_mo | number:2}}</b><br>
                </th>
                <th class="right aligned">
                  <div class="ui tiny statistic left aligned">
                    <div class="value">
                      {{th.slot.tache.t_montant_mo | number:2}}
                    </div>
                  </div>
                </th>
                <th></th>
              </tr>
            </tfoot>
          </table>
        </div>

        <div class="no_break">
          <h4><i class='circular inverted truck icon'></i> Matériels</h4>

          <table  class="ui yellow selectable compact table">
            <thead>
              <tr>
                <th width="">Code</th>
                <th width="100%">Designation</th>
                <th class="center aligned">Nombre</th>
                <th class="right aligned">Amplitude</th>
                <th class="right aligned">Cout horaire</th>
                <th class="right aligned">Montant</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="line_mat in th.slot.tache.materiel">
                <td>{{line_mat.code_mat}}</td>
                <td>{{line_mat.libelle}}</td>
                <td class="center aligned">{{line_mat.nombre | number:0}}</td>
                <td class="right aligned">{{line_mat.amplitude | number:0}}</td>
                <td class="right aligned">{{line_mat.cout_horaire | number:2}}</td>
                <td class="right aligned">{{line_mat.montant | number:2}}</td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <th colspan="4" class="right aligned">
                  Prix horaire du materiel: <b>{{th.slot.tache.prx_h_mat | number:2}}</b><br>
                  Cout du materiel: <b>{{th.slot.tache.cout_mat | number:2}}</b><br>
                </th>
                <th class="right aligned">
                  <div class="ui tiny statistic left aligned">
                    <div class="value">
                      {{th.slot.tache.t_montant_mat | number:2}}
                    </div>
                  </div>
                </th>
                <th></th>
              </tr>
            </tfoot>
          </table>
        </div>

        <div class="no_break">
          <h4><i class='circular inverted puzzle piece icon'></i> Fournitures</h4>

          <table class="ui yellow selectable compact table">
            <thead>
              <tr>
                <th width="">Code</th>
                <th width="100%">Désignation</th>
                <th class="center aligned">Unité</th>
                <th class="right aligned">Quantité</th>
                <th class="right aligned">Cout revient</th>
                <th class="right aligned">Montant</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="line_fourniture in th.slot.tache.fourniture">
                <td>{{line_fourniture.code_fourniture}}</td>
                <td>{{line_fourniture.libelle}}</td>
                <td class="center aligned">{{line_fourniture.um}}</td>
                <td class="right aligned">{{line_fourniture.qte | number:2}}</td>
                <td class="right aligned">{{line_fourniture.cout_revient | number:2}}</td>
                <td class="right aligned">{{line_fourniture.montant | number:2}}</td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <th colspan="4" class="right aligned">
                  Prix unitaire de la fourniture: <b>{{th.slot.tache.prx_u_fourn | number:2}}</b><br>
                  Prix de la fourniture: <b>{{th.slot.tache.prx_fourn | number:2}}</b><br>
                </th>
                <th class="right aligned">
                  <div class="ui tiny statistic left aligned">
                    <div class="value">
                      {{th.slot.tache.prx_u_fourn | number:2}}
                    </div>
                  </div>
                </th>
                <th></th>
              </tr>
            </tfoot>
          </table>
        </div>
        <hr class="style3"></hr>

        <div class="ui inverted  segment right aligned">
          <div class="ui horizontal inverted statistic">
            <div class="label">
              TOTAL DEBOURSE SEC
            </div>
            <div class="value">
              &nbsp;
              {{th.slot.tache.deb_sec | number:2}}
            </div>
          </div>
          <br>
          <div class="ui horizontal inverted statistic">
            <div class="label">
              DEBOURSE SEC ARRONDI
            </div>
            <div class="value">
              &nbsp;
              {{th.slot.tache.deb_sec | number:0}}
            </div>
          </div>
        </div>

      </form>

    </div>
    <div class="actions print_ignore">
      <div class="ui right labeled icon button" ng-click="exportToPrint('#detaille',1)">
        IMPRIMER
        <i class="print icon"></i>
      </div>
    </div>
  </div>

<!-- 
  .oooooo.                                                              o8o      .    o8o
 d8P'  `Y8b                                                             `"'    .o8    `"'
888           .ooooo.  ooo. .oo.  .oo.   oo.ooooo.   .ooooo.   .oooo.o oooo  .o888oo oooo   .ooooo.  ooo. .oo.
888          d88' `88b `888P"Y88bP"Y88b   888' `88b d88' `88b d88(  "8 `888    888   `888  d88' `88b `888P"Y88b
888          888   888  888   888   888   888   888 888   888 `"Y88b.   888    888    888  888   888  888   888
`88b    ooo  888   888  888   888   888   888   888 888   888 o.  )88b  888    888 .  888  888   888  888   888
 `Y8bood8P'  `Y8bod8P' o888o o888o o888o  888bod8P' `Y8bod8P' 8""888P' o888o   "888" o888o `Y8bod8P' o888o o888o
                                          888
                                         o888o
-->


  <div class="ui modal composition" id="composition">
    <div class="ui header">
      <div class="print_ignore">Composition pour</div>
      <div class="sub header">
        {{compose.designation}}
      </div>
    </div>
    <div class="ui scrolling content form">

      <table class="ui very compact fixed single line celled table">
        <thead>
          <tr>
            <th width="50%">Tâche</th>
            <th>Prix U</th>
            <th>Qte</th>
            <th>Montant SEC</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="ele in compose.comp" ng-dblclick="del_comp()">
            <td class="left   aligned">{{ele.tch}}</td>
            <td class="right  aligned">{{ele.pru | number:2}}</td>
            <td class="center aligned">{{ele.qte}}</td>
            <td class="right  aligned">{{ele.mnt | number:2}}</td>
          </tr>
        </tbody>
        <tfoot>
            <th colspan="3"><b>Montant de cette composition</b></th>
            <th class="right aligned">
              <div class="ui tiny statistic"><div class="value">{{compose.deb_sec | number:2}}</div></div>
            </th>
          </tr>
        </tfoot>
      </table>
      
    </div>
    <div class="actions print_ignore">
      <div class="ui button" ng-click="exportToPrint('#composition')">Imprimer</div>
    </div>
  </div>




  <div class="ui modal ext_view">
    <div class="content" id="result"></div>
  </div>

<!-- 

ooooo                           .oooooo.   ooooo   ooooo
`888'                          d8P'  `Y8b  `888'   `888'
 888  ooo. .oo.    .oooo.o    888           888     888
 888  `888P"Y88b  d88(  "8    888           888ooooo888
 888   888   888  `"Y88b.     888           888     888
 888   888   888  o.  )88b    `88b    ooo   888     888
o888o o888o o888o 8""888P'     `Y8bood8P'  o888o   o888o

 -->


  <div class="ui modal ins_CH" id="ins_CH">
    <div class="ui header">
      <div class="print_ignore">Installation Chantier</div>
    </div>

    <div class="ui scrolling content form">          
        <table class="ui celled compact table" ng-cloak>
          <thead><tr><th width="50%">Designation</th><th>Prix U</th><th>UM</th><th width="100px">Qte</th><th>Montant</th></tr></thead>
          <tbody>
            <tr ng-repeat="ele in installation">
              <td>{{ele.des}}</td>
              <td style="text-align:right;">{{ele.prx | number:2}}</td>
              <td>{{ele.um}}</td>
              <td style="text-align: center; width: 100px;">{{ele.qte}}</td>
              <td class="right aligned">{{ele.mnt | number:2}}</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="4"><h4>TOTAL</h4></th>
              <th class="right aligned"><h2>{{ mnt_installation ||0 | number:2}}</h2></th>
            </tr>
          </tfoot>
        </table>
    </div>
    <div class="actions print_ignore">
      <div class="ui button" ng-click="exportToPrint('#ins_CH')">Imprimer</div>
    </div>

  </div>


<!-- 

ooooooooo.              .o8                  o8o
`888   `Y88.           "888                  `"'
 888   .d88'  .oooo.    888oooo.   .oooo.   oooo   .oooo.o
 888ooo88P'  `P  )88b   d88' `88b `P  )88b  `888  d88(  "8
 888`88b.     .oP"888   888   888  .oP"888   888  `"Y88b.
 888  `88b.  d8(  888   888   888 d8(  888   888  o.  )88b
o888o  o888o `Y888""8o  `Y8bod8P' `Y888""8o o888o 8""888P'

 -->


  <div class="ui  modal rabaisModel" id="rabaisModel_">
    <div class="ui header">
      <div class="print_ignore">Rabais</div>
    </div>

    <div class="ui content form" style="min-height: 300px">

      <div class="ui equal width form">
        
        <div class="ui equal width fields">
          <div class="field">
            <label>Type de Rabais</label>
            <select class="ui dropdown" ng-model="thisSdv.rabais.type">
              <option value="">Type de Rabais</option>
              <option value="0">Sans Rabais</option>
              <option value="1">Rabais Commercial</option>
              <option value="2">Rabais Détaillé</option>
            </select>
          </div>

          <div class="two wide field" ng-show="thisSdv.rabais.type==1">
            <label>Taux rabais</label>
            <input type="number" step="any" class="ui input" ng-model="thisSdv.rabais.commercial">
          </div>
        </div>

        <div class="fields" ng-show="thisSdv.rabais.type==2">
            

          <div class="three wide field">
            <label>Lot</label>
            <select class="ui dropdown" ng-model="rabaisLot">
              <option ng-value="value.lot" ng-repeat="(key, value) in thisSdv.xls | groupBy:'lot'">{{value.lot}}</option>
            </select>
          </div>

          <div class="three wide field">
            <label>Sous Lot</label>
            <select class="ui dropdown" ng-model="rabaisSlot">
              <option ng-value="value.slot" ng-repeat="(key, value) in thisSdv.xls | groupBy:'slot' | filter:{lot:rabaisLot}">{{value.slot}}</option>
            </select>
          </div>

          <div class="seven wide field">
            <label>Tâche</label>
            <select class="ui dropdown" ng-model="rabaisTache">
              <option ng-value="value.nLine" ng-repeat="(key, value) in thisSdv.xls | filter:{lot:rabaisLot, slot:rabaisSlot, um_cos:'!!'}">{{value.nArticle}} {{value.designation}}</option>
            </select>
          </div>

          <div class="two wide field">
            <label>Taux rabais</label>
            <input type="text" class="ui input" ng-model="rabaisTaux">
          </div>

          <div class="one wide field">
            <label>&nbsp;</label>
            <div  class="ui icon button" ng-click="addRabais(rabaisLot, rabaisSlot, rabaisTache, rabaisTaux)" 
                  data-inverted="" data-tooltip="Ajouter a la list en bas" data-position="left center">
              <i class="angle double down icon"></i>
            </div>
          </div>
        </div>
        <div class="scrolling content">

          <table class="ui very compact striped table" ng-show="thisSdv.rabais.type==2">
            <thead>
              <tr>
                <th>N</th>
                <th>Lot</th>
                <th>Sous Lot</th>
                <th>Tâche</th>
                <th>Taux</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="(key, value) in thisSdv.rabais.tache">
                <td>{{value.nline}}</td>
                <td>{{value.lot}}</td>
                <td>{{value.slot}}</td>
                <td>{{value.designation}}</td>
                <td>{{value.rabais}}</td>
                <td><div class="ui mini icon button" ng-click="delRabais($index)"><i class="minus icon"></i></div></td>
              </tr>
            </tbody>
          </table>

        </div>

      </div>

      <!--div class="ui scrolling content form" style="min-height: 300px">
      </div-->

    </div>
    <div class="actions print_ignore">
      <div class="ui psitive button" ng-click="setRabais()">Appliquer</div>
    </div>

  </div>

<!-- 
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
 -->




<?=FOOTER_PAGE?>


<!-- 
       oooo     .oooooo..o                     o8o                 .
       `888    d8P'    `Y8                     `"'               .o8
        888    Y88bo.       .ooooo.  oooo d8b oooo  oo.ooooo.  .o888oo
        888     `"Y8888o.  d88' `"Y8 `888""8P `888   888' `88b   888
        888         `"Y88b 888        888      888   888   888   888
        888    oo     .d8P 888   .o8  888      888   888   888   888 .
    .o. 88P    8""88888P'  `Y8bod8P' d888b    o888o  888bod8P'   "888"
    `Y888P                                           888
                                                    o888o
 -->

<script language="javascript"> app.controller('TodoCtrl', function($scope, $filter, $sce, $templateRequest, $compile, $http) {

//$('.ui.page.dimmer').dimmer('show');

  $scope.tmp = {};
  $scope.global_mnt = 0;
  $scope.global_tva = 0;
  $scope.global_construction = 0;

  // LOAD TACHE ///////
  $http.get('api/?tache_flist')
    .then(function(res){
      $scope.tache = res.data;
      console.log('tache',$scope.tache);

      // LOAD PRJ //////////
      $http.get('api/?draft=<?=$num_devis?>&load')
        .then(function(res){
          $scope.devis = res.data;
          $scope.devis.sub_devis.forEach( function(ele) {
            ele.lot_devis = [];
          });

          console.log("Load Devis <?=$num_devis?>", $scope.devis);
          $scope.devis.sub_devis.forEach( function(element, index) {
            element.surfLogts = element.surface_hab || element.nbr_logs ;
            element.tva_etudes = <?=intval($fw->fetchAll("SELECT * FROM parametre WHERE arg='tva des etudes'")[0]->val)?>;
            $scope.recap(element);
          });
    

          // LES SOUS DEVIS SONT INCLUE DANS DEVIS AU MOMENT DU CALCULE KC,KV ADD3

          // // LOAD SUB PRJ //////////
          // <?php 
          //   if ($diff!="")
          //     echo "\$http.get('api/?draft=$num_sub_devis&load')
          //   .then(function(res){
          //     //res.data.tva_devis = parseFloat(res.data.tva_devis ||0);
          //     \$scope.sub_devis = res.data;
          //     console.log(\"Load SubDevis $num_sub_devis\", \$scope.sub_devis);
          //   });";
          // ?>

        });

    })
  ;

  // SAVE /////////////
  $scope.save = function(){
    return $http.post('api/?draft=<?=$num_devis?>&save',$scope.devis)
      .then(function(res){
        console.log('saved [ OK ]');
        $('.ui.page.dimmer').dimmer('hide');

      });
  }
  /////////////////////

/*

ooooooooo.   oooooooooooo   .oooooo.         .o.       ooooooooo.
`888   `Y88. `888'     `8  d8P'  `Y8b       .888.      `888   `Y88.
 888   .d88'  888         888              .8"888.      888   .d88'
 888ooo88P'   888oooo8    888             .8' `888.     888ooo88P'
 888`88b.     888    "    888            .88ooo8888.    888
 888  `88b.   888       o `88b    ooo   .8'     `888.   888
o888o  o888o o888ooooood8  `Y8bood8P'  o88o     o8888o o888o

*/


  $scope.recap = function(sdv){
    console.log("sdv",sdv);
    sdv.lot           = [];
    sdv.nlot          = 0;
    sdv.tlot          = 0;
    sdv.slot          = [];
    sdv.nslot         = 0;
    sdv.tslot         = 0;
    sdv.nack_task     = 0;
    sdv.total_mnt_sec = 0;  // Z mnt * Qte
    sdv.total_mnt     = 0;  // Z mnt * Qte * KV
    sdv.tva           = parseFloat( sdv.tva || 0 );

    var tmp = {
        nlot:0,
        lot:null,
        tlot:0,
        nslot:0,
        slot:null,
        tslot:0
    };

    $scope.msg_error_recap = [];

    var last_index_lot;
    var close_lot = 0;
    var last_index_slot;
    var close_slot = 0;

    sdv.xls.forEach(function (l,i) {


      switch(l.matching) {

        case 'IGNORER':
          break;

        case 'LOT':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = null;
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          
          tmp.nlot       += 1;
          tmp.lot         = (l.nArticle?l.nArticle:'')+' '+(l.designation?l.designation:'');
          tmp.tlot        = 0;

          if (close_slot != 0){
            $scope.msg_error_recap.push('TOTAL SOUS LOT [ '+sdv.slot[last_index_slot].des+' ] n\'ai pas définie');
            close_slot = 0;
          }
          if (close_lot != 0){
            $scope.msg_error_recap.push('TOTAL LOT [ '+sdv.lot[last_index_lot-1].des+' ] n\'ai pas définie');
            close_lot = 0;
          }
          close_lot +=1;

          break;
          
        case 'SOUS LOT':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = null;
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          
          tmp.nslot      += 1;
          tmp.slot        = (l.nArticle?l.nArticle:'')+' '+(l.designation?l.designation:'');
          tmp.tslot       = 0;

          if (close_lot == 0){
            $scope.msg_error_recap.push('À quel LOT appartient '+l.designation+'?');
          }
          if (close_slot != 0){
            $scope.msg_error_recap.push('TOTAL SOUS LOT [ '+tmp.slot+' ] nai pas définie');
            close_slot = 0;
          }
          close_slot +=1;
          break;

        case 'TOTAL LOT':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(tmp.tlot);
          l.kv_lot           = l.matching;
          sdv.tlot       += tmp.tlot;
          l.lot           = null;
          l.slot          = null;
          
          if (close_lot != 1){
            $scope.msg_error_recap.push('Nom LOT non défini pour le TOTAL LOT, [ '+l.designation+' ]');
            close_lot = 1;
          }else{
            sdv.lot.push({des:tmp.lot, mnt:tmp.tlot});
          }
          close_lot -=1;
          break;

        case 'TOTAL SOUS LOT':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(tmp.tslot);
          l.kv_lot        = l.matching;
          sdv.tslot      += tmp.tslot;
          l.lot           = null;
          l.slot          = null;
          
          if (close_slot != 1){
            $scope.msg_error_recap.push('Nom SOUS LOT, [ '+l.designation+' ] non défini');
            close_slot = 1;
          }else{
            sdv.slot.push({des:tmp.slot, mnt:tmp.tslot});
          }
          close_slot -=1;
          break;

        case 'ETUDES':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt:0);
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        case 'ETUDES ESQUISSE':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_esq:0);
          l.kv_lot        = l.matching;
          console.log(sdv.etudes.mnt_esq);
          l.lot           = null;
          l.slot          = null;
          break;

        case 'ETUDES AVANT PROJET':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_avprj:0);
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        case 'ETUDES PROJET EXECUTION':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_prjex:0);
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        case 'ETUDES PLAN DE RECOLLEMENT':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_plrec:0);
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        case 'ETUDES PERMIS DE CONSTRUIRE':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_permis:0);
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        case 'INSTALLATION CHANTIER':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = 0;
          $scope.devis.lot.forEach( function(ln) {
            l.mnt               = l.mnt               || ln.frais.installation_CH
            sdv.installation_CH = sdv.installation_CH || ln.frais.installation_CH
            sdv.installation    = sdv.installation    || ln.frais.installation
          });
          l.kv_lot        = l.matching;
          sdv.total_mnt  += l.mnt;
          l.lot           = null;
          l.slot          = null;
          break;

        case 'NOTIFICATION LOT':
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        //case 'NOTIFICATION TACHE':
          //l.kv_lot        = l.matching;
          //l.lot           = null;
          //l.slot          = null;
          //break;

        case 'TOTAL ETUDES HT':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0);
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        case 'TOTAL ETUDES TVA':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0) * .19;
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        case 'TOTAL ETUDES TTC':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0) + ((sdv.etudes?sdv.etudes.mnt:0) * .19);
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        case 'TOTAL ETUDES ET REALISATION HT':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0) + sdv.total_mnt;
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        case 'TOTAL ETUDES ET REALISATION TVA':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          //l.mnt           = ((sdv.etudes?sdv.etudes.mnt:0) * .19) + ((sdv.etudes?sdv.total_mnt:0) * sdv.tva);
          l.mnt           = ((sdv.etudes?sdv.etudes.mnt:0) * .19) + (sdv.total_mnt * (sdv.tva /100));
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        case 'TOTAL ETUDES ET REALISATION TTC':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          //l.mnt           = (sdv.etudes?sdv.etudes.mnt:0) + ((sdv.etudes?sdv.etudes.mnt:0) * .19) + sdv.total_mnt + (sdv.total_mnt * sdv.tva);
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0) + ((sdv.etudes?sdv.etudes.mnt:0) * .19) + sdv.total_mnt + (sdv.total_mnt * (sdv.tva /100));
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        case 'TOTAL REALISATION HT':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = sdv.total_mnt;
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        case 'TOTAL REALISATION TVA':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = sdv.total_mnt * (sdv.tva /100);
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        case 'TOTAL REALISATION TTC':
          l.deb_sec       = null;
          l.qte           = null;
          l.mnt_sec       = null;
          l.kv            = null;
          l.mnt           = sdv.total_mnt + (sdv.total_mnt * (sdv.tva /100));
          l.kv_lot        = l.matching;
          l.lot           = null;
          l.slot          = null;
          break;

        default:
          l.deb_sec       = parseFloat( l.deb_sec || 0 );
          l.qte           = parseFloat( l.qte || 0 );
          l.mnt_sec       = (l.deb_sec - ( l.deb_sec * ((l.rabais?l.rabais:0) /100) )) * l.qte
          //l.mnt_sec       = l.deb_sec * l.qte
                          // deb-sec - ( deb-sec *      rabais                     ) * qte
          l.kv            = 1;
          l.lot           = tmp.lot;
          l.slot          = tmp.slot;
          console.log('$scope.devis.lot[0].frais.KV', $scope.devis.lot[0].frais.KV);
          if (            $scope.devis.lot[0].frais.KV && $scope.devis.lot[0].frais.KV > 1 ){

                          l.kv  = $scope.devis.lot[0].frais.KV;
                          l.kv_lot = 'GLOBAL';

          // }else if (      $scope.devis.lot[1].frais.KV &&
          //                 $scope.devis.lot[1].frais.KV > 1 &&
          //                 l.num_tache &&
          //                 $scope.tache.find(function(el){return el.num_tache === l.num_tache}).num_lot == "GVRD" 
          //           ) {
          //                 l.kv  = $scope.devis.lot[1].frais.KV;
          //                 l.lot = 'GVDR';

          // }else if (      $scope.devis.lot[2].frais.KV &&
          //                 $scope.devis.lot[2].frais.KV > 1 &&
          //                 l.num_tache &&
          //                 $scope.tache.find(function(el){return el.num_tache === l.num_tache}).num_lot == "CES" 
          //           ) {
          //                 l.kv  = $scope.devis.lot[2].frais.KV;
          //                 l.lot = 'CES';

          // }else if (      $scope.devis.lot[3].frais.KV &&
          //                 $scope.devis.lot[3].frais.KV > 1 &&
          //                 l.num_tache &&
          //                 $scope.tache.find(function(el){return el.num_tache === l.num_tache}).num_lot == "CET" 
          //           ) {
          //                 l.kv  = $scope.devis.lot[3].frais.KV;
          //                 l.lot = 'CET';

          }else{          
                          //(l.nArticle?l.nArticle:'')+' '+(l.designation?l.designation:'')
                          $scope.devis.lot.forEach(function(elot){
                            //console.log(elot.des , '/',l.lot);
                            if (elot.des == l.lot){
                              l.kv  = elot.frais.KV;
                              l.kv_lot = 'LOT CLIENT '+elot.des;
                            } 
                          });                
          }

          if (l.matching == "NOTIFICATION TACHE")
            l.kv_lot = "NOTIFICATION TACHE";

          l.mnt           = l.mnt_sec * l.kv;

          tmp.tlot       += l.mnt;
          tmp.tslot      += l.mnt;
          sdv.total_mnt_sec += l.mnt_sec;
          sdv.total_mnt  += l.mnt;

          if ( !l.matching || l.matching == "" ){
            sdv.nack_task++;
          }

          if ( close_lot == 0 && close_slot == 0 && l.matching && l.matching.indexOf("TOTAL") ){
            $scope.msg_error_recap.push('Tâche [ '+l.designation+' ] n\'appartenant pas à un LOT ou un SOUS LOT');
          }

      //e.mnt                                   = e.deb_sec * e.qte * e.KV;
      //$scope.tmp.lot_kv                       = angular.copy(e.KV);
      //this.$parent.$parent.sdv.total_lot      = parseFloat(this.$parent.$parent.sdv.total_lot      ||0) + e.mnt;
      //this.$parent.$parent.sdv.total_sous_lot = parseFloat(this.$parent.$parent.sdv.total_sous_lot ||0) + e.mnt;
      //this.$parent.$parent.sdv.total_devis    = parseFloat(this.$parent.$parent.sdv.total_devis    ||0) + e.mnt;



      }
    if (sdv.comp && sdv.comp[i])
      l.comp = sdv.comp[i];
    });
  }

  $scope.analyse_f = function(dv){
    console.log('Analyse financiere ',dv);

    // mnt des realisation HT  
    dv.mnt_rl = dv.total_mnt;
    console.log('mnt des realisation HT', dv.mnt_rl);
    // mnt des realisation HT avec rabais  
    dv.mnt_rl_rb = dv.mnt_rl - dv.mnt_rb;
    console.log('mnt des realisation HT avec rabais', dv.mnt_rl_rb);

    // mnt de rabais HT
    if (dv.rabais === undefined || dv.rabais === null)
      dv.rabais = {commercial:0};
    if (dv.rabais.commercial === undefined || dv.rabais.commercial === null)
      dv.rabais.commercial = 0; 
    dv.mnt_rb = dv.mnt_rl * (dv.rabais.commercial /100);
    console.log('mnt de rabais HT', dv.mnt_rb);

    // mnt TVA des realisation 
    dv.mnt_tva = dv.mnt_rl * (dv.tva/100);
    console.log('mnt TVA des realisation', dv.mnt_tva);
    // mnt TVA des realisation avec rabais 
    dv.mnt_tva_rb = dv.mnt_rl_rb * (dv.tva/100);
    console.log('mnt TVA des realisation avec rabais', dv.mnt_tva_rb);

    // mnt des realisation TTC
    dv.mnt_ttc = dv.mnt_rl + dv.mnt_tva;
    console.log('mnt des realisation TTC', dv.mnt_ttc);
    // mnt des realisation TTC avec rabais
    dv.mnt_ttc_rb = dv.mnt_rl_rb + dv.mnt_tva_rb;
    console.log('mnt des realisation TTC avec rabais', dv.mnt_ttc_rb);



    // taux de TVA des etudes
    dv.etudes.tva = dv.tva_etudes;
    console.log('taux de TVA des etudes', dv.etudes.tva);

    // mnt de TVA des etudes 
    dv.etudes.mnt_tva = dv.etudes.mnt * (dv.etudes.tva /100);
    console.log('mnt de TVA des etudes', dv.etudes.mnt_tva);

    // mnt des etudes TTC 
    dv.etudes.mnt_ttc = dv.etudes.mnt + dv.etudes.mnt_tva;
    console.log('mnt des etudes TTC', dv.etudes.mnt_ttc);



    // mnt des etudes et realisation HT
    dv.mnt_et_rl = dv.etudes.mnt + dv.mnt_rl_rb;
    console.log('mnt des etudes et realisation HT', dv.mnt_et_rl);

    // mnt de TVA des etudes et realisation
    dv.mnt_tva_et_rl = dv.etudes.mnt_tva + dv.mnt_tva_rb;
    console.log('mnt de TVA des etudes et realisation', dv.mnt_tva_et_rl);

    // mnt des etudes et realisation TTC
    dv.mnt_ttc_et_rl = dv.mnt_et_rl + dv.mnt_tva_et_rl;
    console.log('mnt des etudes et realisation TTC', dv.mnt_ttc_et_rl);

  };

  $scope.NumberToLetter = function(mnt){
    var v =  mnt.toFixed(2).toString().split(".") ;
    return NumberToLetter(v[0]) + ' Dinars et ' + NumberToLetter(v[1]) + ' Centimes';    
  };


  $scope.exportXls = function(id){
    console.log(this.sdv);
    return $http.post('api/?xlsx='+this.sdv.xls_file+'&export&exto='+this.sdv.obj_devis,this.sdv)
      .then(function(res){
        if (res.data.file)
          location.assign(res.data.file);
      });
  }

  $scope.printElement = function(obj,phf){
    if (phf){
      $('html').css('padding','100px 0 0 0');
      $('.PAPER_HEADER').removeClass('print_ignore');
    }
    
    $(obj).removeClass('print_ignore');
    window.print();
    $(obj).addClass('print_ignore');
    $('html').css('padding','0');
    $('.PAPER_HEADER').addClass('print_ignore');
  }


  $scope.calc_cas_niveau = function(){
    var templateUrl = $sce.getTrustedResourceUrl('template/cas_niveau.html');
    $templateRequest(templateUrl).then(function(template) {
      $scope.est_reg = {};
      $compile($("#result").html(template).contents())($scope);
      $('.ui.modal.ext_view')
        .modal({
          onVisible : function(){
            $scope.est_reg = $scope.regard.est_reg;
          }
        })
        .modal('show')
      ;

    });
  }

  $scope.detaille = function(e){
    if (e.matching == 'INSTALLATION CHANTIER'){
      console.log('INSTALLATION CHANTIER ',this.$parent.$parent.sdv.installation);
      $scope.installation = this.$parent.$parent.sdv.installation;
      $scope.mnt_installation = this.$parent.$parent.sdv.installation_CH;
      $('.ins_CH').modal('show');

    }else if (e.matching == 'COMPOSITION TACHE'){
      console.log('COMPOSITION TACHE ', e);
      $scope.compose = e;
      $('.composition').modal('show');

    }else if (e.matching == 'ESTIMATION DES REGARDS'){
      $scope.regard = e;
      $scope.rOnly = true;
      console.log('ESTIMATION DES REGARDS ', e);
      $scope.calc_cas_niveau();

    }else{
      var th = $scope.tache.find(function(el){return el.num_tache === e.num_tache});
      if (th){
        $http.get('api/?bd&lot='+th.num_lot+'&slot='+th.num_slot+'&tache='+th.num_tache)
          .then(function(res){

            $scope.th              = res.data[0];
            $scope.th.slot         = $scope.th.slot[0];
            $scope.th.slot.tache   = $scope.th.slot.tache[0];
            $scope.th.tache_client = e.designation;
            console.log('TACHE', $scope.th);
            $('.detaille').modal('show');
          })
        ;
      }

    }
  }

  $scope.showRabaisModel = function(tp){
    $scope.thisSdv = this.sdv;
    console.log($scope.thisSdv.rabais);
    $('.rabaisModel').modal('show');
  }

  $scope.addRabais = function( rabaisLot, rabaisSlot, rabaisTache, rabaisTaux ){
    if ( $scope.thisSdv.rabais === undefined)
      $scope.thisSdv.rabais = null;
    if ( $scope.thisSdv.rabais.tache === undefined)
      $scope.thisSdv.rabais.tache = [];

    var list = $scope.thisSdv.rabais.tache;
    var th   = null;

    // Rabais sur tache
    if (rabaisLot && rabaisSlot && rabaisTache != null && rabaisTaux){

      th = list.find(line => line.nLine === rabaisTache );
      if (th !== undefined){
        th.rabais = parseFloat(rabaisTaux);
      }else{
        th = angular.copy( $scope.thisSdv.xls.find(line => line.nLine === rabaisTache ) );
        th.rabais = parseFloat(rabaisTaux);
        list.push(th);
      }

    // Rabais sur sous lot
    }else if (rabaisLot && rabaisSlot && !rabaisTache && rabaisTaux){
      console.log('Rabais par sous lot');
      $scope.thisSdv.xls.forEach( function(el, index) {
        if (el.lot == rabaisLot && el.slot == rabaisSlot && el.um_cos ){
          th = list.find(line => line.nLine === el.nLine );
          if (th !== undefined){
            th.rabais = parseFloat(rabaisTaux);
          }else{
            th = angular.copy( $scope.thisSdv.xls.find(line => line.nLine === el.nLine ) );
            console.log('th', th);
            th.rabais = parseFloat(rabaisTaux);
            list.push(th);
          }
        }
      });
    
    // Rabais sur lot
    }else if (rabaisLot && !rabaisSlot && !rabaisTache && rabaisTaux){
      console.log('Rabais par lot');
      $scope.thisSdv.xls.forEach( function(el, index) {
        if (el.lot == rabaisLot  && el.um_cos){
          th = list.find(line => line.nLine === el.nLine );
          if (th !== undefined){
            th.rabais = parseFloat(rabaisTaux);
          }else{
            th = angular.copy( $scope.thisSdv.xls.find(line => line.nLine === el.nLine ) );
            th.rabais = parseFloat(rabaisTaux);
            list.push(th);
          }
        }
      });

    }
    $scope.rabaisLot   = null;
    $scope.rabaisSlot  = null;
    $scope.rabaisTache = null;
    $scope.rabaisTaux  = null;
  }

  $scope.setRabais = function( index ){

    // Sans Rabais
    if ($scope.thisSdv.rabais.type == 0){
      // Pas de Rabais
      $scope.thisSdv.xls.forEach( function(el, index) {
        if (el.kv !== undefined && el.kv !== null){
          el.rabais = 0;
        }
      });
      $scope.thisSdv.rabais.commercial = 0;
      $scope.thisSdv.rabais.tache = [];

    // Rabais Commercial
    }else if ($scope.thisSdv.rabais.type == 1){
      // Rabais commercial
      $scope.thisSdv.xls.forEach( function(el, index) {
        if (el.kv !== undefined && el.kv !== null){
          el.rabais = 0;
        }
      });

    // Rabais Détaillé
    }else if ($scope.thisSdv.rabais.type == 2){
      // Rabais detailles
      $scope.thisSdv.xls.forEach( function(el, index) {
        if (el.kv !== undefined && el.kv !== null){
          var e = $scope.thisSdv.rabais.tache.find(line => line.nArticle === el.nArticle && line.designation === el.designation);
          if ( e ){
            el.rabais = e.rabais;
          }else{
            el.rabais = 0;
          }
        }
      });
      $scope.thisSdv.rabais.commercial = 0;

    }
    
    // recap, save and exit
    $scope.recap($scope.thisSdv);
    $scope.analyse_f($scope.thisSdv);
    $scope.save();
    $('.rabaisModel').modal('hide');
    //$scope.$apply();
  }

  $scope.delRabais = function( index ){
    $scope.thisSdv.rabais.tache.splice(index, 1);
  }

  $scope.exportToPrint = function(obj,phf){
    console.log('print ',obj);
    var html = '';
    html += '<!DOCTYPE html><html><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><link rel="stylesheet" href="css/semantic-ui/semantic.min.css"><link rel="stylesheet" href="css/print.css"></head><body>';
    html += '<div style="border-bottom:solid 2px #000;width:100%;padding:.2em 0;height:80px;"><img src="img/logo.svg" style="float:left;height:70px;"><img src="img/iso.svg" style="float:right;height:70px;"></div><p></p>';
    html += $(obj).html();
    html += '<script>print();close();<\/script></body></html>';

    w=window.open();
    w.document.write(html);
  }

  $scope.dbg=function(){console.log($scope);$http.post('api/?draft=tmp&save',$scope.devis)}

})
.filter('groupBy', function(){
  return function(list, group_by) {
    var filtered = [];
    angular.forEach(list, function(item) {
      if (item[group_by] && !filtered.find(line => line[group_by] === item[group_by]) )
        filtered.push(item);
    });
    return filtered;
  };
});

$('.ui.page.dimmer').dimmer({
    duration    : {
      show : 500,
      hide : 500
    }
  });
</script>
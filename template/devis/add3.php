<?php // DEFINITION FRAIS CHANCHIER 

$num_sub_devis = sql_inj($_GET['projet'],'');
list($num_devis,$diff) = explode('-', $num_sub_devis."-");

if (!$num_sub_devis){
  $_GET['err']='Erreur 404';
  $_GET['msg']='Projet non trouver';
  include("template/err.php");
  die();
}else if ($diff!=""){
  //include("template/devis/add3.php");
  //die();
}

$adv = $fw->fetchAll("SELECT num_devis FROM devis WHERE num_devis LIKE '$num_devis-%'");

?>
<div ng-controller="TodoCtrl" ng-cloak>
  
  <div class="ui fixed bottom sticky">
    <div class="ui image label" >
      <i class="hotjar icon"></i>
      {{sub_devis.num_devis || devis.num_devis}}
      <div class="detail">{{sub_devis.obj_devis || devis.obj_devis}}</div>
    </div>
  </div>
  
  <?php if (isallow("debug")) echo DEBUG_BUTTON;?>

  <div class="ui attached small steps print_ignore">
    <a class="step" href="?p=devis/add<?=($diff!="")?'1':'0'?>&projet=<?=$num_sub_devis;?>">
      <i class="id card icon"></i>
      <div class="content">
        <div class="title">Projet</div>
        <div class="description">Informations du Projet</div>
      </div>
    </a>      

    <?php
    if ($diff!="")
    {
      echo "
    <a class='step' href='?p=devis/add2&projet=$num_sub_devis'>
      <i class='file excel icon'></i>
      <div class='content'>
        <div class='title'>Devis Quantitatif EXCEL</div>
        <div class='description'>Analyse du Fichier Client</div>
      </div>
    </a>";
    }
    else
    {
      echo '
    <div class="ui step top left pointing dropdown">
      <i class="file excel icon"></i>
      <div class="content">
        <div class="title">Devis Quantitatif EXCEL</div>
        <div class="description">Analyse du Fichier Client</div>
      </div>
      <div class="menu">
        <a class="item" ng-repeat="ele in devis.sub_devis" href="?p=devis/add2&projet={{ele.num_devis}}">
        <i class="icon file"></i>
        {{ele.obj_devis}}</a>
      </div>
    </div>
    ';
    } 

    ?>


    <a class="active red_border step" href="?p=devis/add3&projet=<?=$num_sub_devis;?>">
      <i class="info icon"></i>
      <div class="content">
        <div class="title">Définition des frais</div>
        <div class="description"></div>
      </div>
    </a>

    <div class='ui step top left pointing dropdown disabled'>
      <i class='codepen icon'></i>
      <div class='content'>
        <div class='title'>ETUDES</div>
        <div class='description'></div>
      </div>
      <div class='menu'>
        <a  class='item'
            ng-repeat='ele in devis.sub_devis'
            ng-if='ele.dv_etudes'
            href='?p=devis/add5&projet={{ele.num_devis}}'
        >
          <i class='icon file'></i>
          {{ele.obj_devis}}
        </a>
      </div>
    </div>





    <a class="step" href="?p=devis/add4&projet=<?=$num_sub_devis;?>">
      <i class="calculator icon"></i>
      <div class="content">
        <div class="title">Estimation du DEVIS</div>
        <div class="description"></div>
      </div>
    </a>
  </div>


<?=HEADER_PAGE?>

<datalist id="frais_ins_ch">
    <option data-ng-repeat="ele in tache | filter: {num_lot:'FCH', num_slot:'INS'}" value="{{ele.nom_tache}}"> 
</datalist>

<datalist id="frais_fnc_ch">
    <option data-ng-repeat="ele in tache | filter: {num_lot:'FCH', num_slot:'FCN'}" value="{{ele.nom_tache}}"> 
</datalist>

  <div class="ui raised very padded container piled segment" ng-cloak>

    <form method="post" action="" class="ui form">

      <h3 class="ui ">Définition des frais</h3>
<!--       <div class="grouped fields" ng-class="{disabled:devis.av_ins_ch}">
        <div class="field">
          <div class="ui radio">
            <input type="radio" name="ins_ch" id="insCh" class="hidden" ng-model="devis.av_ins_ch" value="1">
            <label for="insCh">AVEC Installation de chantier</label>
          </div>
        </div>
        <div class="field">
          <div class="ui radio">
            <input type="radio" name="ins_ch" id="nInsCh" class="hidden" ng-model="devis.av_ins_ch" value="0">
            <label for="nInsCh">SANS Installation de chantier</label>
          </div>
        </div>
      </div> -->

      <div class="ui message" ng-class="{info:devis.av_ins_ch, negative:!devis.av_ins_ch}">
        <i class="icon info circle"></i> 
        Devis 
          <b ng-if="devis.av_ins_ch">AVEC</b> 
          <b ng-if="!devis.av_ins_ch">SANS</b> 
        Installation de Chantier
      </div>

      <h3 class="ui ">Choix du LOT</h3>
      <div class="equal width fields">
        <div class="field">
          <select class="ui dropdown" ng-model="selected_lot">
            <option disabled selected value> -- selectioner un lot -- </option>
            <option ng-repeat="ele in devis.lot" value="{{$index}}">{{ele.ref}} {{ele.des}}</option>
          </select>
        </div>
        <div class="field print_ignore">
          <div ng-click='copy_()' class='ui icon button' data-tooltip="Copier les Frais Définis" data-inverted="" ng-class="{'disabled':!devis.lot[selected_lot]}"><i class="copy icon"></i></div>
          <div ng-click='paste_()' class='ui icon button' data-tooltip="Coller les Frais Définis" data-inverted="" ng-class="{'disabled':!copy_paste}"><i class="paste icon"></i></div>
          <div ng-click='erase_()' class='ui icon button' data-tooltip="Effacer les Frais Définis" data-inverted="" ng-class="{'disabled':!devis.lot[selected_lot]}"><i class="erase icon"></i></div>
        </div>
      </div>

      <div class="" id="print_frais_g">
<!--
ooooo                               .oooooo.   ooooo   ooooo
`888'                              d8P'  `Y8b  `888'   `888'
 888  ooo. .oo.    .oooo.o        888           888     888
 888  `888P"Y88b  d88(  "8        888           888ooooo888
 888   888   888  `"Y88b.         888           888     888
 888   888   888  o.  )88b .o.    `88b    ooo   888     888
o888o o888o o888o 8""888P' Y8P     `Y8bood8P'  o888o   o888o



-->
        <h3 class="ui ">Installation Chantier</h3>
        <table class="ui celled compact table" ng-cloak>
          <thead><tr><th width="50%">Designation</th><th>Prix U</th><th>UM</th><th width="100px">Qte</th><th>Montant</th></tr></thead>
          <tbody>
            <tr ng-repeat="ele in devis.lot[selected_lot].frais.installation">
              <td>
                <div class="ui fluid transparent input">
                  <input type="text" data-ng-model="ele.des" list="frais_ins_ch" ng-change="search_complete()">
                </div>
              </td>
              <td class="right aligned">{{ele.prx | number:2}}</td>
              <td>{{ele.um}}</td>
              <td width="100px"><div class="ui fluid transparent input"><input type="number" ng-model="ele.qte" ng-change="calc_mnt(this)"></div></td>
              <td class="right aligned">{{ele.mnt | number:2}}</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="4"><h4>TOTAL</h4></th>
              <th class="right aligned"><h2>{{ devis.lot[selected_lot].frais.installation_CH ||0 | number:2}}</h2></th>
            </tr>
          </tfoot>
        </table>
        <div class="ui basic right aligned segment print_ignore">
          <div class="ui mini primary button" ng-click="add('installation');" ng-class="{disabled:!selected_lot}"><i class='add icon'></i> Ajouter des frais</div>
        </div>

<!-- 
oooooooooooo   .oooooo.   ooooo      ooo   .oooooo.            .oooooo.   ooooo   ooooo
`888'     `8  d8P'  `Y8b  `888b.     `8'  d8P'  `Y8b          d8P'  `Y8b  `888'   `888'
 888         888      888  8 `88b.    8  888                 888           888     888
 888oooo8    888      888  8   `88b.  8  888                 888           888ooooo888
 888    "    888      888  8     `88b.8  888                 888           888     888
 888         `88b    d88'  8       `888  `88b    ooo  .o.    `88b    ooo   888     888
o888o         `Y8bood8P'  o8o        `8   `Y8bood8P'  Y8P     `Y8bood8P'  o888o   o888o

 -->

        <h3 class="ui ">Fonctionnement Chantier</h3>
        <table class="ui celled compact table">
          <thead><tr><th width="50%">Designation</th><th>Prix U</th><th>UM</th><th width="100px">Qte</th><th>Montant</th></tr></thead>
          <tbody>
            <tr ng-repeat="ele in devis.lot[selected_lot].frais.fonctionement">
              <td>
                <div class="ui fluid transparent input">
                  <input type="text" data-ng-model="ele.des" list="frais_fnc_ch" ng-change="search_complete()">
                </div>
              </td>
              <td class="right aligned">{{ele.prx | number:2}}</td>
              <td>{{ele.um}}</td>
              <td><div class="ui fluid transparent input"><input type="number" ng-model="ele.qte" ng-change="calc_mnt(this)"></div></td>
              <td class="right aligned">{{ele.mnt | number:2}}</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="4"><h4>TOTAL</h4></th>
              <th class="right aligned"><h2>{{ devis.lot[selected_lot].frais.fonctionement_CH ||0 | number:2}}</h2></th>
            </tr>
          </tfoot>
        </table>
        <div class="ui basic right aligned segment print_ignore">
          <div class="ui mini primary button" ng-click="add('fonctionement');" ng-class="{disabled:!selected_lot}"><i class='add icon'></i> Ajouter des frais</div>
        </div>


<!-- 
oooooooooooo                     o8o                .oooooo.
`888'     `8                     `"'               d8P'  `Y8b
 888         oooo d8b  .oooo.   oooo   .oooo.o    888
 888oooo8    `888""8P `P  )88b  `888  d88(  "8    888
 888    "     888      .oP"888   888  `"Y88b.     888     ooooo
 888          888     d8(  888   888  o.  )88b    `88.    .88'
o888o        d888b    `Y888""8o o888o 8""888P'     `Y8bood8P'



 -->

        <h3 class="ui ">Frais Généraux</h3>
        <a  class="ui right ribbon blue basic label"
            ng-click="
              devis.lot[selected_lot].frais.generaux = {
                aleas:2,
                fr_sieges:10,
                fr_finance:2,
                fr_gestion:4,
                tap:2,
                benefice:8,
                assurance:2
              };
              devis.lot[selected_lot].frais.frais_generaux = 30;
            "
            >Valeurs par défaut</a>

        <table class="ui celled compact table">
          <tr>
            <td>Aléas:</td>
            <td>
              <div class="ui fluid transparent input"  ng-class="{disabled:!selected_lot}">
                <input type="number" ng-model="devis.lot[selected_lot].frais.generaux.aleas" step="0.01" min="0" max="100" ng-change="calc_this_fee(selected_lot)">
              </div>
            </td>
            <td class="center aligned" width="50px">%</td>
          </tr>

          <tr>
            <td>Frais de Siège</td>
            <td>
              <div class="ui fluid transparent input" ng-class="{disabled:!selected_lot}">
                <input type="number" ng-model="devis.lot[selected_lot].frais.generaux.fr_sieges" step="0.01" min="0" max="100" ng-change="calc_this_fee(selected_lot)">
              </div>
            </td>
            <td class="center aligned">%</td>
          </tr>

          <tr>
            <td>Frais Financiers</td>
            <td>
              <div class="ui fluid transparent input" ng-class="{disabled:!selected_lot}">
                <input type="number" ng-model="devis.lot[selected_lot].frais.generaux.fr_finance" step="0.01" min="0" max="100" ng-change="calc_this_fee(selected_lot)">
              </div>
            </td>
            <td class="center aligned">%</td>
          </tr>

          <tr>
            <td>TAP</td>
            <td>
              <div class="ui fluid transparent input" ng-class="{disabled:!selected_lot}">
                <input type="number" ng-model="devis.lot[selected_lot].frais.generaux.tap" step="0.01" min="0" max="100" ng-change="calc_this_fee(selected_lot)">
              </div>
            </td>
            <td class="center aligned">%</td>
          </tr>

          <tr>
            <td>Bénéfice</td>
            <td>
              <div class="ui fluid transparent input" ng-class="{disabled:!selected_lot}">
                <input type="number" ng-model="devis.lot[selected_lot].frais.generaux.benefice" step="0.01" min="0" max="100" ng-change="calc_this_fee(selected_lot)" toNumber>
              </div>
            </td>
            <td class="center aligned">%</td>
          </tr>

          <tr>
            <td>Assurances</td>
            <td>
              <div class="ui fluid transparent input" ng-class="{disabled:!selected_lot}">
                <input type="number" ng-model="devis.lot[selected_lot].frais.generaux.assurance" step="0.01" min="0" max="100" ng-change="calc_this_fee(selected_lot)">
              </div>
            </td>
            <td class="center aligned">%</td>
          </tr>
          <tr>
            <td>Frais de Gestion</td>
            <td>
              <div class="ui fluid transparent input" ng-class="{disabled:!selected_lot}">
                <input type="number" ng-model="devis.lot[selected_lot].frais.generaux.fr_gestion" step="0.01" min="0" max="100" ng-change="calc_this_fee(selected_lot)">
              </div>
            </td>
            <td class="center aligned">%</td>
          </tr>
          <tfoot>
            <tr>
              <th><h4>Total:</h4></th>
              <th colspan="2" class="center aligned"><h2>{{devis.lot[selected_lot].frais.frais_generaux ||0 | number:2}} %</h2></th>
            </tr>
          </tfoot>

        </table>

      </div>

      <h3 class="ui print_ignore">Planning Attaché</h3>
      <input type="file" name="file" onchange="angular.element(this).scope().upload(this)" >
      <div  class="print_ignore"
            style="
              text-align: center;
              border: 1px #ccc dashed;
              border-top: 0;
            "
            >
        <div class="ui input">
        </div>
        <a  href="{{devis.planning}}" target="_blank" >
          <img src="{{devis.planning}}" style="max-height: 300px; max-width: 100%" alt="">
        </a>
      </div>

      <hr class="style3 print_ignore"></hr>

      <div class="ui basic segment massive aligned print_ignore">

<div class="two fields">
  <div class="field">
    <label> COEFFICIENT DE CHANTIER </label>
    <input  type="number" 
            placeholder="0.00"
            ng-model="devis.lot[selected_lot].frais.KC"
            step=".01"
            style="font-size: 50px; padding: 10px 20px;"
            toNumber>
  </div>

  <div class="field">
    <label> COEFFICIENT DE VENTE </label>
    <input  type="number" 
            placeholder="0.00"
            ng-model="devis.lot[selected_lot].frais.KV"
            step=".01"
            style="font-size: 50px; padding: 10px 20px;"
            toNumber>
  </div>
</div>

      </div>

      <hr class="style3 print_ignore"></hr>

      <div class="ui negative message print_ignore" ng-show="msg_error">
        <!--i class="close icon"></i-->
        <div class="header">
          Erreur 
        </div>
          <p>{{msg_error}}</p>
      </div>


      <div class='ui basic right aligned segment print_ignore'>
       
        <div ng-click='printElement("print_frais_g",1)' class='ui button' ng-class='save.loading' >
          <i class="print icon"></i> Imprimer Frais (Chantier et Généraux)</div>
        
        <!--div ng-click='save()' class='ui button' ng-class='save.loading' >
          <i class="save icon"></i> Enregistrer brouillon</div-->
        
        <div ng-click='calc_INV()' class='ui basic button' >
          <i class="calculator icon"></i> Calcule INVERSES</div>
        
        <!--div ng-click='calc_KV()' class='ui basic red button' >
          <i class="calculator icon"></i> Calculer KV</div>
        <div ng-click='calc_KV()' class='ui basic red button' >
          <i class="calculator icon"></i> Calculer KC+KV</div-->

          <div class="ui buttons">
            <div class="ui active basic button"><i class="calculator icon"></i> Calculer</div>
            <div class="ui basic pink button" ng-click='calc_KV()' > KV</div>
            <div class="ui basic pink button" ng-click='calc_KC_KV()' > KC+KV</div>
            <div class="ui active basic button"></div>
          </div>
        
        <div ng-click='next()' class='ui huge button green' >
          APPLIQUER<!--  ENREGISTRER --></div>
        <p></p>
        <!--div ng-click='debug_()' class='ui youtube button' ng-class='save.loading' >
          <i class="bug icon"></i> View $scope.devis</div-->
      </div>
    </form>

  </div>

<?=FOOTER_PAGE?>

</div>

<!-- 
   oooo  .oooooo..o
   `888 d8P'    `Y8
    888 Y88bo.
    888  `"Y8888o.
    888      `"Y88b
    888 oo     .d8P
.o. 88P 8""88888P'
`Y888P
 -->
<script language="javascript"> app.controller('TodoCtrl', function($scope, $interval, $filter, $http) {
  // $interval(function(){
  //   return $http.post('api/?draft='+$scope.devis.num_devis+'&save',$scope.devis)
  //     .then(function(res){
  //       console.log('save');
  //     });
  // },10000,0);
  // INIT //////////
  var aleas=0;      // 2
  var fr_sieges=0;  // 10
  var fr_finance=0; // 2
  var fr_gestion=0; // 4
  var tap=0;        // 2
  var benefice=0;   // 8
  var assurance=0;  // 2

  var frais = {
    installation:[],
    fonctionement:[],
    generaux:{
      aleas:aleas,
      fr_sieges:fr_sieges,
      fr_finance:fr_finance,
      fr_gestion:fr_gestion,
      tap:tap,
      benefice:benefice,
      assurance:assurance
    },
    installation_CH:0,
    fonctionement_CH:0,
    frais_generaux:0,
    KC:0,
    KV:0
  };

  // LOAD PRJ //////////
  $http.get('api/?draft=<?=$num_devis?>&load')
    .then(function(res){
      $scope.devis = res.data;
      $scope.devis.av_ins_ch = 0;
      $scope.devis.sub_devis = [];
      $scope.devis.total_mnt = 0;

      if (!$scope.devis.lot)
        $scope.devis.lot = [{
          des:'GLOBAL',
          frais:angular.copy(frais)
        // },{
        //   des:'GROS OEUVRE+VRD',
        //   frais:angular.copy(frais)
        // },{
        //   des:'CORPS D\'ETATS SECONDAIRES',
        //   frais:angular.copy(frais)
        // },{
        //   des:'CORPS D\'ETATS TECHNIQUES',
        //   frais:angular.copy(frais)
        }];

      // LOAD ALL Sub DEVIS ARRAY ///////
      <?=json_encode($adv);?>.forEach(function(e,i){
        $http.get('api/?draft='+e.num_devis+'&load')
          .then(function(res){
            console.log('Load Sub Devis '+e.num_devis,res.data);
            $scope.devis.sub_devis.push({
            //$scope.all_sub_devis.push({
              num_devis   :res.data.num_devis,
              nom_devis   :res.data.nom_devis,
              obj_devis   :res.data.obj_devis,
              dv_etudes   :res.data.dv_etudes,
              etudes      :(res.data.dv_etudes ? res.data.etudes : {
                              taux        :0,
                              mnt         :0,
                              mnt_esq     :0,
                              mnt_avprj   :0,
                              mnt_prjex   :0,
                              mnt_plrec   :0,
                              mnt_permis  :0,
                              taux_esq    :0,
                              taux_avprj  :0,
                              taux_prjex  :0,
                              taux_plrec  :0,
                              taux_permis :0
                            }),
              tva         :parseFloat(res.data.tva         ||0),
              nbr_logs    :parseFloat(res.data.nbr_logs    ||0),
              surface_hab :parseFloat(res.data.surface_hab ||0),
              lot         :res.data.lot,
              slot        :res.data.slot,
              comp        :res.data.comp,
              xls_file    :res.data.xls_file,
              xls_header  :res.data.header,
              xls         :res.data.xls,
              bpu         :res.data.bpu,
              SheetNames  :res.data.SheetNames,
              total_mnt   :parseFloat(res.data.total_mnt   ||0)
            });
            $scope.devis.total_mnt += parseFloat(res.data.total_mnt ||0);


            res.data.lot.forEach(function(e,i){
              if ( !$scope.devis.lot.find(function(el){return el.des === e.des && el.ref === e.ref}) ){

                $scope.devis.lot.push({
                  des:e.des,
                  frais:angular.copy(frais)
                });
              }
            });

            res.data.xls.forEach(function(e,i){
              if ( e.matching == 'INSTALLATION CHANTIER' ){
                $scope.devis.av_ins_ch = 1;
              }
            });

          })
        ;
      });

      console.log("Load Devis <?=$num_devis?>", $scope.devis);

    });

  // SAVE /////////////
  $scope.save = function(){
    this.loading = 'loading';
    console.log('save', $scope.devis);
    return $http.post('api/?draft='+$scope.devis.num_devis+'&save',$scope.devis)
      .then(function(res){
        console.log('saved '+$scope.devis.num_devis);
      });
  }

  // NEXT /////////////
  $scope.next = function(){
    $scope.save()
      .then(function(){
        $http.post('api/?update=step3',$scope.devis)
          .then(function(r){
            console.log('result',r);
            if (r.data.res == 'done!')
              location.assign("?p=devis/add4&projet="+$scope.devis.num_devis);
            //else
            //  $scope.msg_error = r.data.pdo.message;

          });
      });
  }
  /////////////////////

  $http.get('api/?tache_flist')
    .then(function(res){
      $scope.tache = res.data;
      console.log("tache", $scope.tache);
    });

  $scope.add = function(e){
    console.log(e);
    if (e == 'installation')
      $scope.devis.lot[$scope.selected_lot].frais.installation.push({des:null,prx:0,um:null,qte:0,mnt:0});
    else
      $scope.devis.lot[$scope.selected_lot].frais.fonctionement.push({des:null,prx:0,um:null,qte:0,mnt:0});
  }

  $scope.search_complete = function(){
    var srh = $filter('filter')($scope.tache, {nom_tache: this.ele.des}, true);
    //console.log(this.ele.des, srh);
    if (srh.length == 1)
    {
      this.ele.prx = srh[0].deb_sec;
      this.ele.um  = srh[0].um;
      this.ele.qte = 1;
      $scope.calc_mnt(this);
    }else{
      this.ele.prx = 0;
      this.ele.um  = null;
      this.ele.qte = 0;
      $scope.calc_mnt(this);
    }
  }



  $scope.calc_mnt = function(e){ // calcule des montant dune ligne
    console.log(e);
    e.ele.mnt = parseFloat(e.ele.prx) * parseFloat(e.ele.qte);
    $scope.calc_this_fee($scope.selected_lot);
  }


  $scope.calc_this_fee = function(e){ // calcule KC

    $scope.devis.lot[e].frais.installation_CH = 0;
    $scope.devis.lot[e].frais.installation.forEach(function(ele){
      $scope.devis.lot[e].frais.installation_CH += parseFloat(ele.mnt);
    });

    $scope.devis.lot[e].frais.fonctionement_CH = 0;
    $scope.devis.lot[e].frais.fonctionement.forEach(function(ele){
      $scope.devis.lot[e].frais.fonctionement_CH += parseFloat(ele.mnt);
    });
    
    $scope.devis.lot[e].frais.frais_generaux =   
      parseFloat($scope.devis.lot[e].frais.generaux.aleas      || 0)+
      parseFloat($scope.devis.lot[e].frais.generaux.fr_sieges  || 0)+
      parseFloat($scope.devis.lot[e].frais.generaux.fr_finance || 0)+
      parseFloat($scope.devis.lot[e].frais.generaux.fr_gestion || 0)+
      parseFloat($scope.devis.lot[e].frais.generaux.tap        || 0)+
      parseFloat($scope.devis.lot[e].frais.generaux.benefice   || 0)+
      parseFloat($scope.devis.lot[e].frais.generaux.assurance  || 0);

  }

  $scope.calc_INV = function(){ // calcule Inverses des frais généraux
    $scope.msg_error = null;

    $scope.devis.lot[$scope.selected_lot].frais.frais_generaux = (1 - ($scope.devis.lot[$scope.selected_lot].frais.KC / $scope.devis.lot[$scope.selected_lot].frais.KV)) * 100;
    
    let benefice = $scope.devis.lot[$scope.selected_lot].frais.frais_generaux - (
            $scope.devis.lot[$scope.selected_lot].frais.generaux.aleas +
            $scope.devis.lot[$scope.selected_lot].frais.generaux.fr_sieges +
            $scope.devis.lot[$scope.selected_lot].frais.generaux.fr_finance +
            $scope.devis.lot[$scope.selected_lot].frais.generaux.fr_gestion +
            $scope.devis.lot[$scope.selected_lot].frais.generaux.tap +
            $scope.devis.lot[$scope.selected_lot].frais.generaux.assurance
          );

    console.log(benefice);
    $scope.devis.lot[$scope.selected_lot].frais.generaux.benefice = parseFloat(benefice.toFixed(2));
    $scope.save();
  }

  $scope.calc_KV = function(){ // calcule K Vente

    $scope.devis.lot.forEach(function(e,i){
      // Koefficient de vente
      var kv = e.frais.KC / (1 - (e.frais.frais_generaux/100));
      e.frais.KV = angular.copy(parseFloat(kv.toFixed(2)));
      console.log('KV', kv);
    });

    $scope.save();

  }
  $scope.calc_KC_KV = function(){ // calcule K Vente

    console.log($scope.devis.av_ins_ch);

    if ($scope.devis.av_ins_ch === undefined){
      $scope.msg_error = "Veuillez choisir votre mode calcule, AVEC ou SANS installation de chantier";
    }
    else
    {
      $scope.msg_error = null;

      var sum_frais_installation  = 0;
      var sum_frais_fonctionement = 0;
      var sum_frais_generale      = 0;

      console.log('============================================');
      console.log('TOTAL Qte * DebSEC des tache', $scope.devis.total_mnt);

      if ($scope.devis.av_ins_ch == "1" ){
        console.log('Devis avec installation de chantier');
      }else{
        console.log('Devis sans installation de chantier');
      }

      $scope.devis.lot.forEach(function(e,i){

        $scope.calc_this_fee(i);

        console.log('============================================');
        console.log('LOT: ',        e.ref + ' ' + e.des);
        console.log('installation', e.frais.installation_CH);
        console.log('fonctionement',e.frais.fonctionement_CH);
        console.log('generale',     e.frais.frais_generaux);
        console.log('DEB SEC ',     $scope.devis.total_mnt);

        // Koefficient de chantier

        var kc = 0;
        if ($scope.devis.av_ins_ch == "1" ) // devis avec installation de chantier
          kc = (parseFloat($scope.devis.total_mnt ||0) + (e.frais.fonctionement_CH)) / parseFloat($scope.devis.total_mnt ||0);
        else // devis sans installation de chantier
          kc = (parseFloat($scope.devis.total_mnt ||0) + (e.frais.installation_CH + e.frais.fonctionement_CH)) / parseFloat($scope.devis.total_mnt ||0);

        // Koefficient de vente
        var kv = kc / (1 - (e.frais.frais_generaux/100));

        e.frais.KC = angular.copy(parseFloat(kc.toFixed(2)));
        e.frais.KV = angular.copy(parseFloat(kv.toFixed(2)));

        console.log('KC', kc);
        console.log('KV', kv);

      });
    }
    $scope.save();
  }


  $scope.copy_ =function(){
    $scope.copy_paste = angular.copy($scope.devis.lot[$scope.selected_lot].frais);
    console.log('copy_',$scope.copy_paste);
  }
  $scope.paste_=function(){
    $scope.devis.lot[$scope.selected_lot].frais = angular.copy($scope.copy_paste);
    console.log('paste_', $scope.devis.lot[$scope.selected_lot]); 
  }
  $scope.erase_=function(){
    $scope.devis.lot[$scope.selected_lot].frais = {
      installation:[],
      fonctionement:[],
      generaux:{
        aleas:aleas,
        fr_sieges:fr_sieges,
        fr_finance:fr_finance,
        fr_gestion:fr_gestion,
        tap:tap,
        benefice:benefice,
        assurance:assurance
      },
      installation_CH:0,
      fonctionement_CH:0,
      frais_generaux:0,
      KC:0, 
      KV:0 
    };
    console.log('erase_',$scope.devis.lot[$scope.selected_lot]); 
  }
  $scope.dbg   =function(){
    console.log(this.devis);
    $http.post('api/?draft=tmp&save',$scope.devis)
  }


  $scope.printElement = function(obj,phf){
    if (phf){
      $('html').css('padding','100px 0 0 0');
      $('.PAPER_HEADER').removeClass('print_ignore');
    }
    
    $('#'+obj).removeClass('print_ignore');
    window.print();
    $('#'+obj).addClass('print_ignore');
    $('html').css('padding','0');
    $('.PAPER_HEADER').addClass('print_ignore');
  }


  // upload file whene input change 
  $scope.upload = function(el){
    var fileData = new FormData();
    fileData.append('file', el.files[0]);
    
    //console.log(fileData);
    $http.post('api/?upload=img',fileData, {
      headers: {
        'Content-Type': undefined
      },
      transformResponse: angular.identity
    })
    .then(function(r){
      $scope.devis.planning = JSON.parse(r.data).fullpath;
      console.log($scope.devis.planning);
    });
  }

}); 

$('.ui.radio.checkbox')
  .checkbox()
;

webshims.setOptions('forms-ext', {
    replaceUI: 'auto',
    types: 'number'
});
webshims.polyfill('forms forms-ext');
</script>

<?php // ETUDES

$num_sub_devis = sql_inj($_GET['projet'],'');
list($num_devis,$diff) = explode('-', $num_sub_devis."-");

?>



<div ng-controller="TodoCtrl" ng-cloak>
  <?php if (isallow("debug")) echo DEBUG_BUTTON;?>
  <div class="ui fixed bottom sticky">
    <div class="ui image label" >
      <i class="hotjar icon"></i>
      {{sub_devis.num_devis}}
      <div class="detail">{{sub_devis.obj_devis}}</div>
    </div>
  </div>
  <div class="ui attached small steps print_ignore">
    <a class="step" href="?p=devis/add1&projet=<?=$num_sub_devis;?>">
      <i class="id card icon"></i>
      <div class="content">
        <div class="title">Projet</div>
        <div class="description">Informations du Projet</div>
      </div>
    </a>
    <a class="step" href="?p=devis/add2&projet=<?=$num_sub_devis;?>">
      <i class="file excel icon"></i>
      <div class="content">
        <div class="title">Devis Quantitatif EXCEL</div>
        <div class="description">Analyse du Fichier Client</div>
      </div>
    </a>
    <a class="step" href="?p=devis/add3&projet=<?=$num_sub_devis;?>">
      <i class="info icon"></i>
      <div class="content">
        <div class="title">Définition des frais</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="active red_border step"  href="?p=devis/add5&projet=<?=$num_sub_devis?>"
       ng-class="{disabled:!sub_devis.dv_etudes}">
      <i class="codepen icon"></i>
      <div class="content">
        <div class="title">ETUDES</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="step" href="?p=devis/add4&projet=<?=$num_sub_devis;?>">
      <i class="calculator icon"></i>
      <div class="content">
        <div class="title">Estimation du DEVIS</div>
        <div class="description"></div>
      </div>
    </a>
  </div>

<?=HEADER_PAGE?>

<div class="ui raised very padded container piled segment" ng-cloak>

    <form method="post" action="" id="etudes" class="ui form">

      <div class="ui basic segment">
        PROJET : <b>{{sub_devis.num_devis}} / {{sub_devis.nom_devis}}</b> <br>
        Objet : <b>{{sub_devis.obj_devis}}</b> <br>
      </div>
      <h3 class="ui ">Devis avec Etudes</h3>
      <table class="ui definition celled compact table">
        <thead>
          <tr>
            <th width="33%"></th>
            <th width="33%">Taux</th>
            <th width="33%">Montant</th>
          </tr>
        </thead>
        <tbody>
            
          <tr>
            <td style="border-bottom:2px dashed #f88">Etudes</td>
            <td style="border-bottom:2px dashed #f88">

              <div class="ui fluid transparent input">
                <input  type="number" 
                        ng-model="sub_devis.etudes.taux" 
                        ng-change="calc_mnt()" 
                        min="0" 
                        max="100"
                        >
              </div>

            </td>
            <td style="border-bottom:2px dashed #f88">
              <div class="ui fluid transparent input">
                <input type="number" ng-model="sub_devis.etudes.mnt" min="0" step="0.01">
              </div>
            </td>
          </tr>

          <tr>
            <td>Esquisse</td>
            <td>
              <div class="ui fluid transparent input">
                <input type="number" ng-model="sub_devis.etudes.taux_esq" min="0" max="100" step="0.1">
              </div>
            </td>
            <td>{{ sub_devis.etudes.mnt_esq = (sub_devis.etudes.taux_esq * sub_devis.etudes.mnt) / 100 | number:2 }} </td>
          </tr>

          <tr>
            <td>Avant projet</td>
            <td>
              <div class="ui fluid transparent input">
                <input type="number" ng-model="sub_devis.etudes.taux_avprj" min="0" max="100" step="0.1">
              </div>
            </td>
            <td>{{ sub_devis.etudes.mnt_avprj = (sub_devis.etudes.taux_avprj * sub_devis.etudes.mnt) / 100 | number:2 }} </td>
          </tr>

          <tr>
            <td>Projet Execution</td>
            <td>
              <div class="ui fluid transparent input">
                <input type="number" ng-model="sub_devis.etudes.taux_prjex" min="0" max="100" step="0.1">
              </div>
            </td>
            <td>{{ sub_devis.etudes.mnt_prjex = (sub_devis.etudes.taux_prjex * sub_devis.etudes.mnt) / 100 | number:2 }} </td>
          </tr>

          <tr>
            <td>Plan de recollement</td>
            <td>
              <div class="ui fluid transparent input">
                <input type="number" ng-model="sub_devis.etudes.taux_plrec" min="0" max="100" step="0.1">
              </div>
            </td>
            <td>{{ sub_devis.etudes.mnt_plrec = (sub_devis.etudes.taux_plrec * sub_devis.etudes.mnt) / 100 | number:2 }} </td>
          </tr>

          <tr>
            <td>Dossier de Permis de Construire</td>
            <td>
              <div class="ui fluid transparent input">
                <input type="number" ng-model="sub_devis.etudes.taux_permis" min="0" max="100" step="0.1">
              </div>
            </td>
            <td>{{ sub_devis.etudes.mnt_permis = (sub_devis.etudes.taux_permis * sub_devis.etudes.mnt) / 100 | number:2 }} </td>
          </tr>

        </tbody>
      </table>




      <div class="ui negative message" ng-show="msg_error">
        <!--i class="close icon"></i-->
        <div class="header">
          Erreur 
        </div>
          <p>{{msg_error}}</p>
      </div>


      <div class='ui basic right aligned segment print_ignore'>
       
        <div class='ui button' ng-click="printElement('#etudes',1)"><i class="print icon"></i> Imprimer</div>
        
        <div class='ui teal button' ng-class="{loading:save_button}" ng-click='save()'><i class="save icon"></i> Enregistrer</div>
        
      </div>
    </form>

  </div>

  <?=FOOTER_PAGE?>

</div>
<script language="javascript">app.controller('TodoCtrl', function($scope, $filter, $http, $location){

  // LOAD PRJ //////////
  $http.get('api/?draft=<?=$num_devis?>&load')
    .then(function(res_devis){
      $scope.devis = res_devis.data;
      $scope.srh = $filter('filter')(res_devis.data.sub_devis, {num_devis: '<?=$num_sub_devis?>' }, true);
      //$scope.total_devis = srh[0].total_devis;
      

      $http.get('api/?draft=<?=$num_sub_devis?>&load')
        .then(function(res_sdevis){
          $scope.sub_devis = res_sdevis.data;
          //$scope.sub_devis.total_devis = srh[0].total_devis;
          if (!$scope.sub_devis.etudes)
            $scope.sub_devis.etudes = {
              taux        :0,
              mnt         :0,
              mnt_esq     :0,
              mnt_avprj   :0,
              mnt_prjex   :0,
              mnt_plrec   :0,
              mnt_permis  :0,
              taux_esq    :0,
              taux_avprj  :0,
              taux_prjex  :0,
              taux_plrec  :0,
              taux_permis :0
            };
          console.log("Load Sous Devis (<?=$num_sub_devis?>) a partire du Devis (<?=$num_devis?>)", $scope.sub_devis);
        });
    });

  // SAVE /////////////
  $scope.save = function(){
    $scope.save_button = true;
    return $http.post('api/?draft='+$scope.sub_devis.num_devis+'&save',$scope.sub_devis)
      .then(function(res){
        
        //console.log($scope.srh);
        $scope.srh[0].etudes = $scope.sub_devis.etudes;
        //console.log($scope.devis);
        //return;

        $http.post('api/?draft=<?=$num_devis?>&save',$scope.devis)
          .then(function(res){
            console.log('Save [ OK ]');
            $scope.save_button = false;
            location.assign("?p=devis/add4&projet="+$scope.devis.num_devis);
          });

      });
  }

  $scope.calc_mnt=function(){
    $scope.sub_devis.etudes.mnt = parseFloat(($scope.sub_devis.total_mnt * ($scope.sub_devis.etudes.taux/100)).toFixed(2));
  }

  $scope.printElement = function(obj,phf){
    if (phf){
      $('html').css('padding','100px 0 0 0');
      $('.PAPER_HEADER').removeClass('print_ignore');
    }
    
    $(obj).removeClass('print_ignore');
    window.print();
    $(obj).addClass('print_ignore');
    $('html').css('padding','0');
    $('.PAPER_HEADER').addClass('print_ignore');
  }

  $scope.dbg=function(){console.log($scope);$http.post('api/?draft=tmp&save',$scope.devis)}




});

webshims.setOptions('forms-ext', {
    replaceUI: 'auto',
    types: 'number'
});
webshims.polyfill('forms forms-ext');

</script>

















<?php // BPU

$num_sub_devis = sql_inj($_GET['projet'],'');
list($num_devis,$diff) = explode('-', $num_sub_devis.'-');

if (!$num_devis){
  $_GET['err']='Erreur 404';
  $_GET['msg']='Page not found';
  include("template/err.php");
  die();
}

?>

<?=HEADER_PAGE?>

<div ng-controller="TodoCtrl" ng-cloak>

  <?php if (isallow("debug")) echo DEBUG_BUTTON;?>

  <div class="ui fixed bottom sticky print_ignore">
    <div class="ui image label" >
      <i class="hotjar icon"></i>
      {{sub_devis.num_devis || devis.num_devis}}
      <div class="detail">{{sub_devis.obj_devis || devis.obj_devis}}</div>
    </div>
  </div>

  <div class="ui attached small steps print_ignore">
    <a class="step" href="?p=devis/add1&projet=<?=$num_sub_devis;?>">
      <i class="id card icon"></i>
      <div class="content">
        <div class="title">Projet</div>
        <div class="description">Informations du Projet</div>
      </div>
    </a>
    <div class="ui step top left pointing dropdown">
      <i class="file excel icon"></i>
      <div class="content">
        <div class="title">Devis Quantitatif EXCEL</div>
        <div class="description">Analyse du Fichier Client</div>
      </div>
      <div class="menu">
        <a class="item" ng-repeat="ele in devis.sub_devis" href="?p=devis/add2&projet={{ele.num_devis}}">
        <i class="icon file"></i>
        {{ele.num_devis}} {{ele.nom_devis}}</a>
      </div>
    </div>
    <a class="step" href="?p=devis/add3&projet=<?=$num_sub_devis;?>">
      <i class="info icon"></i>
      <div class="content">
        <div class="title">Définition des frais</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="step"  href="?p=devis/add5&projet=<?=$num_sub_devis?>"
       ng-class="{disabled:!sub_devis.dv_etudes}">
      <i class="codepen icon"></i>
      <div class="content">
        <div class="title">ETUDES</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="step" href="?p=devis/add4&projet=<?=$num_sub_devis;?>">
      <i class="calculator icon"></i>
      <div class="content">
        <div class="title">Estimation du DEVIS</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="active red_border step" href="?p=devis/add6&projet=<?=$num_sub_devis;?>">
      <i class="map icon"></i>
      <div class="content">
        <div class="title">B.P.U</div>
        <div class="description"></div>
      </div>
    </a>
  </div>




<div class="ui basic segment" ng-repeat="sdv in devis.sub_devis" ng-init="$last && dropdown();">
  
<!-- 

oooooooooo.      ooooooooo.       ooooo     ooo
`888'   `Y8b     `888   `Y88.     `888'     `8'
 888     888      888   .d88'      888       8
 888oooo888'      888ooo88P'       888       8
 888    `88b      888              888       8
 888    .88P .o.  888         .o.  `88.    .8'  .o.
o888bood8P'  Y8P o888o        Y8P    `YbodP'    Y8P

 -->

    <input  type="file"
            name="file"
            id="files"
            accept=".xlsx"
            style="visibility:hidden;"
            class="print_ignore" 
            onchange="angular.element(this).scope().uploadBPU(this)"
            >
              
    <div class="ui clearing inverted segment print_ignore">
      
      <h3 class="ui left floated header">
        {{sdv.num_devis}} / {{sdv.obj_devis}}
      </h3>

      <button class="ui right floated inverted button"
              ng-click="printElement('#dv_'+$id)"
              ><i class="print icon"></i> IMPRIMER
      </button>

      <div    class="ui dropdown right floated inverted olive button"
              ng-click="exportXls()"
              ng-class="{disabled:!sdv.BpuSheetName}"
              ><i class="file excel outline icon"></i> Exporter vers EXCEL
      </div>


      <div class="ui top left pointing dropdown right floated inverted olive button">
        <i class="file excel outline icon"></i> Générer un BPU
        <div class="menu">
          <div class="header">
            <i class="file excel icon"></i> 
            Sélectionner Emplacement BPU
          </div>
          <div class="divider"></div>
          <div class="item" ng-click="genBpu(sdv, value);" ng-repeat="(key, value) in sdv.SheetNames">
            {{value}}
          </div>
          <div class="divider"></div>
          
          <label for="files" class="item">
            <i class="map icon"></i> Sélectionner BPU externe
          </label>
        </div>
      </div>


    </div>

    <table  class='ui striped compact celled table print_ignore' 
            ng-init="sdv.total_devis=0" 
            id="dv_{{::$id}}"
            ng-cloak>

      <thead>
        <tr>
          <th>N</th>
          <th width='50%'>Designation</th>
          <th>Um</th>
          <th>Prix U</th>
          <th>Prix U</th>
          <!--th class="active print_ignore"></th-->
        </tr>
      </thead>
      <tbody>
        <!--tr ng-repeat="ele in sdv.xls" ng-if="ele.matching != 'IGNORER' && ele.nArticle != null && ele.designation != null && ele.qte != null" ng-init="total(ele)"-->
        <tr ng-repeat="ele in sdv.xls" ng-if="ele.um_cos && ele.um_cos != ''" ng-init="total(ele)">
          <td>{{ ele.nArticle }}</td>
          <td>
            <div
              style="
                max-width: 350px;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                font-weight: bold;
              ">
              {{ ele.designation }}
            </div>
            <div>
              {{ ele.bpuDes }}
            </div>
          </td>
          <td>{{ ele.um_cos }}</td>
          <td>{{ ele.bpuMnt | number:0 }}</td>
          <td>{{ ele.bpuStr = UmToLetter(ele.um_cos) + (ele.bpuMnt!=null?NumberToLetter(ele.bpuMnt):'') }}</td>

          <!--td class="active print_ignore">{{ ele.deb_sec * ele.kv | number:2}}</td-->
        </tr>
      <tbody>
    </table>

     


  </div>



</div>

<script src="./js/UmToLetter.js"></script>
<script src="./js/natural.js"></script>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $filter, $http) {

  // LOAD TACHE ///////
  $http.get('api/?tache_flist')
    .then(function(res){
      $scope.tache = res.data;
      console.log('tache',$scope.tache);

      // LOAD PRJ //////////
      $http.get('api/?draft=<?=$num_devis?>&load')
        .then(function(res){
          $scope.devis = res.data;
          console.log('devis',$scope.devis);

          $scope.dropdown();
        });

    })
  ;


  $scope.NumberToLetter = function(mnt){
    var v =  mnt.toFixed(0).toString().split(".") ;
    return NumberToLetter(v[0]) + ' Dinars ';    
  };

  $scope.UmToLetter = function(um){
    return (UmToLetter(um)?UmToLetter(um)+' : ':'');    
  };

  $scope.printElement = function(obj,phf){
    if (phf){
      $('html').css('padding','100px 0 0 0');
      $('.PAPER_HEADER').removeClass('print_ignore');
    }
    
    $(obj).removeClass('print_ignore');
    window.print();
    $(obj).addClass('print_ignore');
    $('html').css('padding','0');
    $('.PAPER_HEADER').addClass('print_ignore');
  }


  // upload file whene input change 
  $scope.uploadBPU = function(el){
    var SheetNamesNdx = this.$index;
    var fileData = new FormData();
    fileData.append('file', el.files[0]);
    
    //console.log(fileData);
    $http({
      method: 'POST',
      url: 'api/?upload=bpu',
      headers: {
        'Content-Type': undefined
      },
      data: fileData,
      transformResponse: angular.identity
    })
    .then(function(r){
      $scope.devis.sub_devis[SheetNamesNdx].BpuFile = JSON.parse(r.data).fullpath;
      console.log("UPLOAD FILE" ,$scope.devis.sub_devis[SheetNamesNdx].BpuFile);
      $http.get("api/?xlsx="+$scope.devis.sub_devis[SheetNamesNdx].BpuFile)
        .then(function(res){
          $scope.devis.sub_devis[SheetNamesNdx].SheetNames = res.data.listSheetNames;

          //console.log("BpuSheetNames" ,$scope.devis.sub_devis[SheetNamesNdx].BpuSheetNames);
          $scope.dropdown();

        });
    });
  }

  $scope.genBpu = function(sdv, sheet){
    sdv.BpuSheetName = sheet;
    if (!sdv.BpuFile) sdv.BpuFile = sdv.xls_file;
    $http.get("api/?xlsx="+sdv.BpuFile+"&WorkSheetName="+sdv.BpuSheetName)
      .then(function(res){
//        console.log('bpu file', res.data);
        var srh;

        sdv.xls.forEach( function(el) {
//          console.log(el.bpuMnt, el.designation);
          if (el.designation && el.bpuMnt != null){

            srh = $filter('filter')(res.data.xls, {nArticle: el.nArticle }, true);

//            console.log('i fond ', srh);

            if (srh){
              var nat=0,bnat=0,bpuDes=null,bpuLine=null;
              srh.forEach( function(element, index) {
                if (element.designation){
                  nat = natural.JaroWinklerDistance(element.designation,el.designation); 
                  if (nat > 0.5 && nat > bnat){
                    bnat = nat;
                    bpuDes = element.designation;
                    bpuLine = element.nLine;
                  }
                }

              });
              if (bpuDes){
                el.bpuDes = bpuDes;
                el.bpuLine = bpuLine;
              }

//              console.log(bnat, bpuDes);
              
            }
          }

        });
          
      });


  }

  $scope.exportXls = function(){
    console.log(this.sdv.BpuSheetName);
    var exportTo = this.sdv.BpuFile?this.sdv.BpuFile:this.sdv.xls_file;

    return $http.post('api/?xlsx='+exportTo+'&WorkSheetName='+this.sdv.BpuSheetName+'&exportBpu', this.sdv)
      .then(function(res){
        if (res.data.file)
          location.assign(res.data.file);
      });
  }

  $scope.total = function(e){

    if ( e.matching == "LOT")
    {
      $scope.total_lot = 0;
    }
    else if ( e.matching == "SOUS LOT")
    {
      $scope.total_slot = 0;
    }
    else if ( e.matching == "TOTAL LOT")
    {
      e.bpuMnt = angular.copy($scope.total_lot);
    }
    else if ( e.matching == "TOTAL SOUS LOT")
    {
      e.bpuMnt = angular.copy($scope.total_slot);
    }
    else if ( e.matching != "IGNORER" )
    {
      e.bpuMnt =  e.deb_sec * e.kv ;
      $scope.total_lot  += e.bpuMnt;
      $scope.total_slot += e.bpuMnt;
    }else{
      e.bpuMnt = null;
    }
  }


  $scope.dropdown = function(){
    $('.ui.dropdown')
      .dropdown()
    ;
  }


  $scope.dbg=function(){console.log($scope);$http.post('api/?draft=tmp&save',$scope.devis)}

}); 

</script>
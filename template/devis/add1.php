<?php

$num_sub_devis = sql_inj($_GET['projet'],'');
list($num_devis,$diff) = explode('-', $num_sub_devis.'-new');
if ($diff == 'new' || $diff == '' ){
  //include("template/devis/add0.php");
  //die();
}

if (!$num_devis){
  $_GET['err']='Erreur 404';
  $_GET['msg']='Projet non trouver';
  include("template/err.php");
  die();
}

$sdv = null;
$sdv = $fw->fetchAll("SELECT * FROM devis WHERE num_devis='$num_sub_devis'");

$sdv = !empty($sdv) ? $sdv[0] : (object)[
  "num_devis"=>null,
  "nom_devis"=>null,
  "obj_devis"=>null,
  "num_client"=>null,
  "adresse"=>null,
  "delai_reali"=>0,
  "tva"=>null,
  "surface_hab"=>0,
  "nbr_logs"=>0,
  "fichier_excel"=>null,
  "utilisateur"=>$_SESSION['user']->id_user,
  "group_utilisateur"=>null,  
  "query"=>"insert"
];

$sdv->code_sdevis      = $diff=='new' ? $fw->next_id('devis') : $diff;
$sdv->delai_reali      = intval($sdv->delai_reali);
$sdv->surface_hab      = intval($sdv->surface_hab);
$sdv->nbr_logs         = intval($sdv->nbr_logs);

?>
<div ng-controller="TodoCtrl" ng-cloak>

  <div class="ui fixed bottom sticky">
    <div class="ui image label" >
      <i class="hotjar icon"></i>
      {{sub_devis.num_devis}}
      <div class="detail">{{sub_devis.obj_devis}}</div>
    </div>
  </div>

  <div class="ui attached small steps">

    <a class="active red_border step" href="?p=devis/add1&projet=<?=$num_sub_devis;?>">
      <i class="id card icon"></i>
      <div class="content">
        <div class="title">Projet</div>
        <div class="description">Informations du Projet</div>
      </div>
    </a>
    <?php
    if ($diff!="" && $diff!="new" )
    {
      echo "
    <a class='step' href='?p=devis/add2&projet=$num_sub_devis'>
      <i class='file excel icon'></i>
      <div class='content'>
        <div class='title'>Devis Quantitatif EXCEL</div>
        <div class='description'>Analyse du Fichier Client</div>
      </div>
    </a>";
    }
    else
    {
      echo '
    <div class="ui step top left pointing dropdown">
      <i class="file excel icon"></i>
      <div class="content">
        <div class="title">Devis Quantitatif EXCEL</div>
        <div class="description">Analyse du Fichier Client</div>
      </div>
      <div class="menu">
        <a class="item" ng-repeat="ele in devis.sub_devis" href="?p=devis/add2&projet={{ele.num_devis}}">
        <i class="icon file"></i>
        {{ele.num_devis}} {{ele.nom_devis}}</a>
      </div>
    </div>
    ';
    } 

    ?>
    <a class="step" href="?p=devis/add3&projet=<?=$num_sub_devis;?>" ng-class="{disabled:!sub_devis.xls}">
      <i class="info icon"></i>
      <div class="content">
        <div class="title">Définition des frais</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="step"  href="?p=devis/add5&projet=<?=$num_sub_devis?>" ng-class="{disabled:!sub_devis.dv_etudes}">
      <i class="codepen icon"></i>
      <div class="content">
        <div class="title">ETUDES</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="step" href="?p=devis/add4&projet=<?=$num_sub_devis;?>" ng-class="{disabled:!sub_devis.xls}">
      <i class="calculator icon"></i>
      <div class="content">
        <div class="title">Estimation du DEVIS</div>
        <div class="description"></div>
      </div>
    </a>
  </div>


  <div class="ui raised very padded text container piled segment">


    <div class="ui right dividing rail">
      <div class="ui mini celled list">
        
        <?php

          $user = $fw->getUser($sdv->utilisateur);
          echo "
        <div class='item' style='margin: 7px; padding: 7px;' ng-cloak>
          <img class='ui avatar image' src='$user->avatar'>
          <div class='content'>
            <div class='header'>$user->nom $user->pnom</div>
            $user->ch
          </div>
        </div>";

          $contributeur = json_decode( str_replace( '\"', '"', sql_inj($sdv->group_utilisateur ) ) );
          if ($contributeur){
            foreach ($contributeur as &$value) {
              $user = $fw->getUser($value);
              echo "
        <div class='item' style='margin: 7px; padding: 7px;' ng-cloak>
          <img class='ui avatar image' src='$user->avatar'>
          <div class='content'>
            <div class='header'>$user->nom $user->pnom</div>
            $user->ch
          </div>
        </div>";        
            }
          }

          if ( isallow("admin") || isallow("user_plus") ){
            echo "</div><div align='center'><a class='circular ui icon button mini basic' href='?p=devis/devis_user_rule'><i class='icon circular user link '></i> Ajouter un collaborateur </a>";
          }

        ?>
      </div>
    </div>


    <form class="ui form">

      <div class="equal width fields">
        <div class="five wide field">
          <label>NUMERO</label>
          <input type="text" ng-model="devis.num_devis" class="uppercase" readonly>
        </div>

        <div class="field">
          <label>INTITULÉ</label>
          <input type="text" ng-model="devis.nom_devis" readonly>
        </div>
      </div>

      <div class="equal width fields">
        <div class="field">
          <label>Objet du marché</label>
          <input type="text" ng-model="devis.obj_devis" readonly>
        </div>
      </div>

      <div class="equal width fields">
        <div class="field">
          <label>CLIENT</label>
          <select class="ui dropdown disabled" ng-model="devis.num_client" ng-options="obj.num_client as obj.nom_client for obj in client"></select>
        </div>
      </div>

      <div class="field">
        <label>ADRESSE DU PROJET</label>
        <textarea ng-model="devis.adresse" rows="4" readonly></textarea>
      </div>

      <div class="field">
        <label>Mode de passation</label>
        <input type="text" ng-model="devis.mode_pass" readonly>
      </div>

    </form>
    <hr class="style3"></hr>
    <!-- name="sub_devis" id="sub_devis" ng-model="sub_devis"  -->
    <form class="ui form" ng-submit="submit()">

      <div class="equal width fields">
        <div class="five wide field" ng-cloak>
          <label>NUMERO</label>
          <div class="ui labeled input">
            <div class="ui label">{{devis.num_devis}}-</div>
            <input type="text" ng-model="sub_devis.code_sdevis" class="uppercase" maxlength="255"
                ng-readonly="sub_devis.num_devis" readonly>
          </div>
        </div>

        <div class="field">
          <label>Objet</label>
          <input type="text" ng-model="sub_devis.obj_devis" maxlength="255" required>
        </div>
      </div>
    
      <div class="field">
        <label>TVA %</label>

        <select class="ui dropdown fluid" ng-model="sub_devis.tva" required>
          <?php $tva = $fw->fetchAll("SELECT * FROM tva");
            foreach ($tva as $key => $value) {
              echo "<option value='$value->tva'>$value->tva</option>";
            };?>
        </select>
        
      </div>

      <div class="field">
        <label>Délai de réalisation</label>
        <div class="ui right labeled input">
          <input type="number" ng-model="sub_devis.delai_reali" min="0" maxlength="255" toNumber required>
          <div class="ui basic label">
            MOIS
          </div>
        </div>
      </div>

      <div class="equal width fields">
        <div class="field" ng-class="{disabled: sub_devis.surface_hab}">
          <label>Nombre de Logements</label>
          <input type="number" ng-model="sub_devis.nbr_logs" min="0" maxlength="255" toNumber>
        </div>
        <div class="field" ng-class="{disabled: sub_devis.nbr_logs}"">
          <label>Total Surface {{sub_devis.nature_surface}}</label>

          <!--div class="ui right labeled action input">
            <input type="number" ng-model="sub_devis.surface_hab">
            <div class="ui basic label">
              m2
            </div>
          </div-->


          <div class="ui action input">
            <input type="number" ng-model="sub_devis.surface_hab" min="0" maxlength="255" step="any" toNumber>
            <select class="ui compact selection dropdown" ng-model="sub_devis.nature_surface">
              <option value="habitable">habitable</option>
              <option value="construite">construite</option>
              <option value="utile">utile</option>
            </select>
            <div class="ui disabled button">M²</div>
          </div>

        </div>
      </div>
      
      <div class="item">
        <div class="ui checkbox">
          <input type="checkbox" id="etudes" ng-model="sub_devis.dv_etudes">
          <label for="etudes">Devis avec Etudes</label>
        </div>
      </div>

      <div class="ui negative message" ng-show="msg_error">
        <i class="close icon"></i>
        <div class="header">
          Erreur 
        </div>
          <p>{{msg_error}}</p>
      </div>

      <div class='ui basic right aligned segment'>
        <button class="ui teal button" type="submit"><i class="arrow right icon"></i> Suivant </button>
      </div>


    </form>
  </div>


<script language="javascript"> app.controller('TodoCtrl', function($scope, $filter, $http) {
  // DEFAULTS PARAM /////
  $scope.mode_pass=[{value:'1',label:'Gré à Gré'},{value:'2',label:'Appel d\'offres'},{value:'3',label:'Consultation'}];
  $scope.client = <?=json_encode($fw->fetchAll("SELECT num_client,nom_client FROM client"));?>;
  
  // LOAD PRJ //////////
  $http.get('api/?draft=<?=$num_devis?>&load')
    .then(function(res){
      $scope.devis = res.data;
      console.log("Load Devis <?=$num_devis?>", $scope.devis);
    });

  // LOAD SUB PRJ //////////
  $http.get('api/?draft=<?=$num_sub_devis?>&load')
    .then(function(res){
      if (res.data.num_devis){
        $scope.sub_devis = res.data;
        $scope.sub_devis.query = 'update';
      }else{
        $scope.sub_devis = <?=json_encode($sdv);?>;
      }
      //$('#tva').val() = $scope.sub_devis.tva;

      console.log("Load Sous Devis <?=$num_sub_devis?>", $scope.sub_devis);
    });

  // SAVE /////////////
  $scope.save = function(){
    return $http.post('api/?draft='+$scope.sub_devis.num_devis+'&save',$scope.sub_devis)
      .then(function(res){
        console.log('Save '+$scope.sub_devis.num_devis);
      });
  }

  // NEXT /////////////
  $scope.submit = function(){
    $scope.msg_error = null;
    $scope.sub_devis.nom_devis = $scope.devis.nom_devis;
    if (!$scope.sub_devis.num_devis)
      $scope.sub_devis.num_devis = $scope.devis.num_devis + '-' + $scope.sub_devis.code_sdevis.toUpperCase();
    //$scope.sub_devis.tva = $('#tva').val();

    $scope.save()
      .then(function(){
        $http.post('api/?update=step1',{devis:$scope.devis, sdevis:$scope.sub_devis})
          .then(function(r){
            console.log('result',$scope.sub_devis);
            if (r.data.res == 'done!'){
              location.assign("?p=devis/add2&projet="+$scope.sub_devis.num_devis);
            }else{
              $scope.msg_error = r.data.pdo.message;
              $scope.sub_devis.num_devis = null;
            }
          });
      });
  }
  /////////////////////
}); 

</script>
</div>
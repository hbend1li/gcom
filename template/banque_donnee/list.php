
<div ng-controller="TodoCtrl" >
  
<div ng-repeat="line_lot in bd" ng-cloak ng-init="$last ? rebuild_dropdown() : null" id="lot_{{::$id}}">
  <table class="ui selectable red compact table">
    <thead>
      <tr>
        <th>

          <div class="ui dropdown">
            <h2>{{line_lot.num_lot}}/{{line_lot.nom_lot}}</h2>
            <div class="menu">
              <!--div class="item" ng-click="lot_upd()"><i class="pencil icon"></i> Modifier</div-->
              <div class="item" ng-click="lot_del()" ng-show="line_lot.slot.length==0"><i class="remove icon"></i> Supprimer</div>
            </div>
          </div>

        </th>
        <th class="right aligned print_ignore">

          <div class="ui mini icon button"
                  data-inverted=""
                  data-position="bottom right"
                  data-tooltip="Imprimer ..."
                  ng-click="printElement('#lot_'+$id,1)"><i class="print icon"></i>
          </div>

          <div class="ui mini icon input">
            <input type="text" placeholder="Recherche ..." ng-model="r_tache" >
            <i class="search icon"></i>
          </div>

          <div class='ui mini red icon button'
            data-tooltip="Ajouter un SOUS LOT"
            data-inverted=""
            data-position="left center"
            ng-click="slot_upd(false);"
            >
            <i class='add icon'></i>
          </div>



        </th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="line_slot in line_lot.slot | filter:r_sous_lot"
          ng-init="$last ? accordion_ref() : null"
          ng-cloak
        >
        <td colspan="2">
          <div class='ui accordion' id="tache_{{::$id}}">
            <div class='title' ng-class="{'active':line_lot.slot.length == 1}">
              <h4 class="boundary">
                <i class='dropdown icon'></i>
                {{line_slot.num_slot}}/{{line_slot.nom_slot}}
                <i class='edit icon print_ignore' ng-click="slot_upd(true)"></i>
              </h4>
            </div>
            <div class='content'  ng-class="{'active':line_lot.slot.length == 1}">

              <table class='ui olive table' style="page-break-inside: always;">
                <thead>
                  <tr>
                    <th>Code</th>
                    <th>Designation</th>
                    <th class="center aligned">TU</th>
                    <th class="right aligned">Capacité</th>
                    <th class="right aligned">Prix/H MO</th>
                    <th class="right aligned">Cout MO</th>
                    <th class="right aligned">Prix/H Materiel</th>
                    <th class="right aligned">Cout Materiel</th>
                    <th class="right aligned">Prix Fourn</th>
                    <th class="right aligned">Debourse Sec</th>
                    <th class="right aligned print_ignore" width="100px">
                      <a class='ui mini basic red icon button' ng-click="printElement('#tache_'+$id)">
                        <i class='print icon'></i>
                      </a>

                      <a href="?p=tache&lot={{line_slot.num_lot}}&slot={{line_slot.num_slot}}">
                        <div class='ui mini basic red icon button'
                          data-tooltip="Ajouter une tache"
                          data-variation="mini"
                          data-inverted=""
                          data-position="left center"
                          >
                          <i class='add icon'></i>
                        </div>
                      </a>
                    </th>
                  </tr>
                </thead>
                <tbody>
                    <!-- ng-click="cs_detail(line_lot,line_slot);" -->
                  <tr
                    ng-repeat="line_tache in line_slot.tache | filter:r_tache" 
                    ng-init="$last ? accordion_ref() : null"
                    ng-class="{error:line_tache.agree!='1'}"
                    ng-cloak
                    >
                    <td class="uppercase" ng-dblclick="delete_tache(this)">
                      {{line_tache.num_tache}}
                    </td>
                    <td>{{line_tache.nom_tache}}</td>
                    <td class="center aligned">{{line_tache.tmp_u    | number:2}}</td>
                    <td class="right aligned">{{line_tache.vol_h_mo  | number:2}}</td>
                    <td class="right aligned">{{line_tache.prx_h_mo  | number:2}}</td>
                    <td class="right aligned">{{line_tache.cout_mo   | number:2}}</td>
                    <td class="right aligned">{{line_tache.prx_h_mat | number:2}}</td>
                    <td class="right aligned">{{line_tache.cout_mat  | number:2}}</td>
                    <td class="right aligned">{{line_tache.prx_fourn | number:2}}</td>
                    <td class="right aligned">{{line_tache.deb_sec   | number:2}}</td>
                    <td class="right aligned print_ignore">
                        <a href="?p=tache&lot={{line_slot.num_lot}}&slot={{line_slot.num_slot}}&tache={{line_tache.num_tache}}">
                          <div class='ui mini icon button'
                            data-tooltip="Modifier une tache"
                            data-variation="mini"
                            data-inverted=""
                            data-position="left center"
                            >
                            <i class='ellipsis horizontal icon'></i>
                          </div>
                        </a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
  </div>

  <!-- Ajouter / Modifier un sous_lot -->
  <form autocomplete="off">
    <div class="ui modal souslot">
      <div class="header">
        {{add_souslot.titel}} un SOUS LOT Associé à {{add_souslot.nom_lot}}
      </div>

      <div class="ui equal width form content">
        <div class="fields">
          <div class="three wide field" ng-class="{'error':!add_souslot.num_slot}">
            <label>Code</label>
            <input type="text" ng-model="add_souslot.num_slot" autocomplete="off" required ng-readonly="add_souslot.titel=='Modifier'" class="uppercase" >
          </div>
          <div class="field" ng-class="{'error':!add_souslot.nom_slot}">
            <label>Designation</label>
            <input type="text" ng-model="add_souslot.nom_slot" autocomplete="off" required>
          </div>
        </div>
      </div>

      <div class="actions">
        <div class="ui black icon button" ng-show="add_souslot.titel=='Modifier'; add_souslot.tache.length==0;" ng-click="slot_del()"><i class="trash icon"></i></div>
        <div class="ui black deny button">Annuler</div>
        <button class="ui green button" type="submit" ng-click="update('sous_lot',add_souslot)">{{add_souslot.titel}}</button>
      </div>
    </div>
  </form>







</div>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $http) {

  $http.get('api/?bd<?=isset($_GET["lot"])?'&lot='.$_GET["lot"]:'';?>')
    .then(function(res){
      $scope.bd = res.data;
      console.log("bd", $scope.bd);
    });
     
  $scope.accordion_ref = function(){
    $('.ui.accordion').accordion();
    $('.boundary').popup({delay: {show: 2000, hide: 5000 }});
  }
  
  $scope.rebuild_dropdown = function(){
    $('.dropdown')
      .dropdown();
  }


  $scope.slot_upd = function(upd){
    $scope.loading = '';
    if (upd){
      console.log(this.line_slot);
      $scope.add_souslot = this.line_slot;
      $scope.add_souslot.titel = 'Modifier';
    }else{
      $scope.add_souslot = this.line_lot;
      $scope.add_souslot.titel = 'Ajouter';
    }
    $('.ui.modal.souslot').modal('show');
  }

  $scope.slot_del = function(){
    console.log('delete ', this.add_souslot.num_slot);
    if (confirm("Etre vous sure de vouloir supprimer le sous lot "+this.add_souslot.nom_slot))
    {
      $http.post('api/?delete=sous_lot&w=num_slot&e='+this.add_souslot.num_slot)
      .then(function(r){
        console.log('result',r);
        if (r.data.res = "done!" && r.data.pdo.sqlState == "00000")
          location.reload();
      });
    }
  }
  
  $scope.update = function(tab,frm){
    console.log('update',tab,'with',frm);
    $http.post('api/?update='+tab,frm)
    .then(function(r){
      console.log('result',r);
      if (r.data.res = "done!" && r.data.pdo.sqlState == "00000")
        location.reload();
    });
  }

  $scope.lot_del = function(){
    console.log('delete ', this.line_lot.num_lot);
    if (confirm("Etre vous sure de vouloir supprimer la tache "+this.line_lot.nom_lot))
    {
      $http.post('api/?delete=lot&w=num_lot&e='+this.line_lot.num_lot)
      .then(function(r){
        console.log('result',r);
        if (r.data.res = "done!" && r.data.pdo.sqlState == "00000")
          location.assign("?");
      });
    }
  }


  $scope.printElement = function(obj,phf){
    console.log('print ',obj);
    var html = '';
    html += '<!DOCTYPE html><html><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><link rel="stylesheet" href="css/semantic-ui/semantic.min.css"><link rel="stylesheet" href="css/print.css"></head><body>';
    html += '<div style="border-bottom:solid 2px #000;width:100%;padding:.2em 0;height:80px;"><img src="img/logo.svg" style="float:left;height:70px;"><img src="img/iso.svg" style="float:right;height:70px;"></div><p></p>';
    html += $(obj).html();
    html += '<script>print();close();<\/script></body></html>';

    w=window.open();
    w.document.write(html);
  }

}); </script>

<?=FOOTER_PAGE?>



  

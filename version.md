## TODO:
* Notification
* Ajouter: detaille dune compositionsur sur Estimation du devis
* Edition / Document d'acompagnement 
* Mise a jour les devis quantitatif
* Edition les devis quantitatif par CET
* Ajout les formule sur les fichier Excel exporter

## Historique des changements


**18/04/2018** .. **24/04/2018**
* Rebuild new notification module with MARKDOWN capability.

**18/04/2018**
* upload xlsx file with update last file and auto match.

**17/04/2018**
* les Cout de **MO** **MAT** et **FOUR** sont les plus significatife que le **TOTAL** sur la fiche de **TACHE**
* 4 chiffre aprai la vergule dans Qte, Fournitures, Tache

**16/04/2018**
* Ajouter [E-Mail](https://imail/) sur le menu Déconexion
* Ajouter Dépo de fichier sur le menu Déconexion

**15/04/2018**
* fix Notification
* fix KV sur Estimation du devis
* Mise a jour banque de donner